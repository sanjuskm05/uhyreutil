package com.uhyre.common.service;

import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.context.ApplicationContext;

import com.uhyre.common.dao.UhyreDao;
import com.uhyre.common.dto.BMIInputDto;
import com.uhyre.common.dto.BMIOutputDto;
import com.uhyre.common.dto.PriceOptInputDto;
import com.uhyre.common.dto.PriceOptOutputDto;
import com.uhyre.common.exceptions.UhyreException;
import com.uhyre.common.pojo.CityInfo;
import com.uhyre.common.util.ResponseUtil;
import com.uhyre.common.util.UhyreUtil;

/**
 * 
 * @author Sanjay
 *
 */
public class UhyreServiceImpl implements UhyreService {

	/**
	 * fetch the related skills when given a skill name
	 * 
	 * @param skill String
	 * @return ArrayList<String>
	 */
	@Override
	public ArrayList<String> getRelatedSkills(String skill) throws SQLException {
		ArrayList<String> relatedSkills = new ArrayList<String>(20);
		ApplicationContext appContext = UhyreUtil.getApplicationContext();
		UhyreDao uhyreDao = (UhyreDao) appContext.getBean("uhyreDAO");
		relatedSkills = uhyreDao.getRelatedSkills(skill);
		
		return relatedSkills;
	}
	
	/**
	 * fetch the latitude and longitude when given a city name or zipcode
	 * 
	 * @param cityName
	 * @param cityZipcode
	 * @return CityInfo
	 */
	@Override
	public CityInfo getCityInformation(String cityName, String stateCode, String cityZipcode) throws SQLException {

		ApplicationContext appContext = UhyreUtil.getApplicationContext();
		UhyreDao uhyreDao = (UhyreDao) appContext.getBean("uhyreDAO");
		CityInfo cityInfo = uhyreDao.getCityInformation(cityName, stateCode, cityZipcode);
		
		return cityInfo;
	}
	
	/**
	 * Calculates BMI score for given information of candidate 
	 * 
	 * @param bmiInputData BMIInputDto
	 * @return BMIOutputDto
	 * @throws SQLException 
	 * @throws UhyreException 
	 */
	@Override
	public BMIOutputDto getCandidateBMI(BMIInputDto bmiInputData) throws SQLException, UhyreException{
		BMIOutputDto bmiScore = new BMIOutputDto();
		double candidateEcoMatcherScore = UhyreUtil.getCandidateEconomicScore(bmiInputData.getCandidateEcoData(), 
				bmiInputData.getEmployerEcoData());
		bmiScore.setCandidateEcoMatcherScore(candidateEcoMatcherScore);
		
		double candidateExpMatcherScore = UhyreUtil.getCandidateExpMatcherScore(bmiInputData.getCandidateRelevantExperienceInYears(), 
				bmiInputData.getEmployerMandatoryMinExperienceInYears(),
				bmiInputData.getEmployerMandatoryMaxExperienceInYears());
		bmiScore.setCandidateExpMatcherScore(candidateExpMatcherScore/UhyreUtil.getEmployerExpMatcherScore() * 100);
		
		double candidatePersonalityTraitMatcherScore = 0;
		if(bmiInputData.getEmployerRequirePersonalityTraits() != null){
			candidatePersonalityTraitMatcherScore = UhyreUtil.getCandidatePersonalityTraitMatcherScore(bmiInputData.getCandidatePersonalityTraits(),
					bmiInputData.getEmployerRequirePersonalityTraits());
			bmiScore.setCandidatePersonalityTraitMatcherScore(candidatePersonalityTraitMatcherScore/400 * 100);
		}
		
		double candidateSkillMatcherScore = UhyreUtil.getCandidateSkillMatcherScore(bmiInputData.getEmployerMandatorySkills().getSkills(), 
				bmiInputData.getEmployerNiceToHaveSkills().getSkills(),
				bmiInputData.getCandidateSkills().getSkills());
		bmiScore.setCandidateSkillMatcherScore(candidateSkillMatcherScore / UhyreUtil.getEmployerSkillMatcherScore(bmiInputData.getEmployerMandatorySkills().getSkills(), 
				bmiInputData.getEmployerNiceToHaveSkills().getSkills()) * 100);
		
		double candidateBMIScore = 0;
		if(candidatePersonalityTraitMatcherScore != 0){
			candidateBMIScore = ((candidateSkillMatcherScore + candidateExpMatcherScore)
					/ (UhyreUtil.getEmployerSkillMatcherScore(bmiInputData.getEmployerMandatorySkills().getSkills(), bmiInputData.getEmployerNiceToHaveSkills().getSkills()) + UhyreUtil.getEmployerExpMatcherScore())) * 65 
					+
					candidatePersonalityTraitMatcherScore / 400 * 25
					+
					candidateEcoMatcherScore / 100 * 10
					;
		}else{
			candidateBMIScore = ((candidateSkillMatcherScore + candidateExpMatcherScore)
					/ (UhyreUtil.getEmployerSkillMatcherScore(bmiInputData.getEmployerMandatorySkills().getSkills(), bmiInputData.getEmployerNiceToHaveSkills().getSkills()) + UhyreUtil.getEmployerExpMatcherScore())) * 75 
					+
					candidateEcoMatcherScore / 100 * 25
					;
		}
		
		bmiScore.setCandidateBMIScore(candidateBMIScore);
		return bmiScore;
	}

	@Override
	public PriceOptOutputDto getOptimumPrices(PriceOptInputDto priceOptInputData) throws SQLException, UhyreException {
		PriceOptOutputDto priceOptOutputData = new PriceOptOutputDto();
		
		double empActRate = priceOptInputData.getEmployerActualRate();
		double candMinRate = priceOptInputData.getCandidateMinRate();
		double candAskRate = priceOptInputData.getCandidateAskingRate();
		double uhyreCommissionInPercentage = priceOptInputData.getUhyreCommissionInPercentage();
		
		double candidateUhyreOptimizeRate = 0;	
		double uhyreCommissionInDollars = 0;
		double employerUhyreOptimizeRate = 0;
		double employerBenefitInPercentage = 0;
		double candidateBenefitOverMinInPercentage = 0;
		double candidateBenefitOverAskingRateInPercentage = 0;
		
		if( empActRate > candMinRate) {
			//employerUhyreOptimizeRate calculation 
			if(UhyreUtil.getAverage(empActRate, candAskRate) - (empActRate*uhyreCommissionInPercentage/100) >= empActRate) {
				if(empActRate - (empActRate*uhyreCommissionInPercentage/100) > candMinRate) {
					employerUhyreOptimizeRate = empActRate - (empActRate*uhyreCommissionInPercentage/100);
				}else {
					employerUhyreOptimizeRate = empActRate;
				}
			}else {
				employerUhyreOptimizeRate = UhyreUtil.getAverage(empActRate, candAskRate);				
			}

			//uhyreCommissionInDollars
			if(employerUhyreOptimizeRate - (employerUhyreOptimizeRate*uhyreCommissionInPercentage/100) > candMinRate) {
				uhyreCommissionInDollars = uhyreCommissionInPercentage * employerUhyreOptimizeRate /100;
			}else {
				uhyreCommissionInDollars = employerUhyreOptimizeRate - candMinRate;
			}
			
			//candidateUhyreOptimizeRate
			candidateUhyreOptimizeRate = employerUhyreOptimizeRate - uhyreCommissionInDollars;
			
			//employerBenefitInPercentage
			employerBenefitInPercentage = (empActRate - employerUhyreOptimizeRate)/empActRate * 100;
			
			//candidateBenefitOverMinInPercentage
			candidateBenefitOverMinInPercentage = (candidateUhyreOptimizeRate - candMinRate)/candMinRate * 100;
			
			//candidateBenefitOverAskingRateInPercentage
			candidateBenefitOverAskingRateInPercentage = (candidateUhyreOptimizeRate - candAskRate)/candAskRate * 100;
			
			priceOptOutputData.setUhyreCommissionInDollars(uhyreCommissionInDollars);
			priceOptOutputData.setEmployerUhyreOptimizeRate(employerUhyreOptimizeRate);
			priceOptOutputData.setEmployerBenefitInPercentage(employerBenefitInPercentage);
			priceOptOutputData.setCandidateUhyreOptimizeRate(candidateUhyreOptimizeRate);
			priceOptOutputData.setCandidateBenefitOverMinInPercentage(candidateBenefitOverMinInPercentage);
			priceOptOutputData.setCandidateBenefitOverAskingRateInPercentage(candidateBenefitOverAskingRateInPercentage);
			
			priceOptOutputData.setStatus(ResponseUtil.SUCCESS);
			priceOptOutputData.setMessage(ResponseUtil.SUCCESS);

		}else {
			priceOptOutputData.setStatus(ResponseUtil.SUCCESS);
			priceOptOutputData.setMessage("No Deal !!! Candidate rate is more than employer.");
		}
		
		
		return priceOptOutputData;
	}

	
	
}
