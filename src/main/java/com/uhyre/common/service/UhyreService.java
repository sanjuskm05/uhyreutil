package com.uhyre.common.service;

import java.sql.SQLException;
import java.util.ArrayList;

import com.uhyre.common.dto.BMIInputDto;
import com.uhyre.common.dto.BMIOutputDto;
import com.uhyre.common.dto.PriceOptInputDto;
import com.uhyre.common.dto.PriceOptOutputDto;
import com.uhyre.common.exceptions.UhyreException;
import com.uhyre.common.pojo.CityInfo;

/**
 * 
 * @author Sanjay
 *
 */
public interface UhyreService{
	  /**
	   * 
	   * @param skill
	   * @return
	   * @throws SQLException
	   */
	  public ArrayList<String> getRelatedSkills(String skill) throws SQLException;
	  
	  /**
	   * 
	   * @param cityName
	   * @param stateCode
	   * @param cityZipcode
	   * @return
	   * @throws SQLException
	   */
	  public CityInfo getCityInformation(String cityName, String stateCode, String cityZipcode) throws SQLException;
	  
	  /**
	   * 
	   * @param bmiInputData
	   * @return
	   * @throws SQLException
	   * @throws UhyreException
	   */
	  public BMIOutputDto getCandidateBMI(BMIInputDto bmiInputData) throws SQLException, UhyreException;
	  
	  /**
	   * 
	   * @param priceOptInputData
	   * @return
	   * @throws SQLException
	   * @throws UhyreException
	   */
	  public PriceOptOutputDto getOptimumPrices(PriceOptInputDto priceOptInputData) throws SQLException, UhyreException;
}