package com.uhyre.common.controller;

import java.util.ArrayList;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.uhyre.common.dto.GeneralResponseDto;
import com.uhyre.common.dto.SkillRelationDto;
import com.uhyre.common.service.UhyreService;
import com.uhyre.common.service.UhyreServiceImpl;
import com.uhyre.common.util.LogUtil;
import com.uhyre.common.util.UhyreUtil;
import com.uhyre.common.util.ResponseUtil;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

/**
 * 
 * @author Sanjay
 *
 */

@RestController
public class UhyreSkillController {
	
//	@Autowired
//	UhyreService uhyreService;

	// Example http://localhost:8080/uhyreutil/skills/relationships
	@RequestMapping(value = "/skills/relationships", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<SkillRelationDto> getSkillRelationships(@RequestBody SkillRelationDto skill) {
		try {
			LogUtil.LOG.info(
					"Uhyre getSkillRelationships, Request :\n" + UhyreUtil.getGsonInstance().toJson(skill));
			UhyreService uhyreService = new UhyreServiceImpl();
			ArrayList<String> relatedSkillList = uhyreService.getRelatedSkills(skill.getSkillName());
			if (relatedSkillList != null) {
				skill.setRelatedSkills(relatedSkillList);
				skill.setStatus(ResponseUtil.SUCCESS);
				skill.setMessage(ResponseUtil.SUCCESS);
			} else {
				skill.setRelatedSkills(null);
				skill.setStatus(ResponseUtil.FAILURE_STATUS);
				skill.setMessage(UhyreUtil.TECH_ERROR);
			}
			
		} catch (Exception ex) {
			LogUtil.LOG.warn(ex.getMessage());
			ex.printStackTrace();
			LogUtil.LOG.warn("Exception BAD_REQUEST - " + HttpStatus.BAD_REQUEST.value());
			return new ResponseEntity<SkillRelationDto>(skill, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<SkillRelationDto>(skill, HttpStatus.OK);
	}

	// Example http://localhost:8080/uhyreutil/skills/serverStatus
	@RequestMapping(value = "/skills/serverStatus", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<GeneralResponseDto> getServerStatus() {
		GeneralResponseDto data = new GeneralResponseDto();
		try {
			data.setMessage("Alive!!!");
			data.setStatus("Success");
		} catch (Exception ex) {
			return new ResponseEntity<GeneralResponseDto>(data, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<GeneralResponseDto>(data, HttpStatus.OK);
	}
}