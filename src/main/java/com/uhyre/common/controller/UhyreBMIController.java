package com.uhyre.common.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.uhyre.common.dto.BMIInputDto;
import com.uhyre.common.dto.BMIOutputDto;
import com.uhyre.common.service.UhyreService;
import com.uhyre.common.service.UhyreServiceImpl;
import com.uhyre.common.util.LogUtil;
import com.uhyre.common.util.UhyreUtil;
import com.uhyre.common.util.ResponseUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

/**
 * 
 * @author Sanjay
 *
 */

@RestController
public class UhyreBMIController {

	// Example http://localhost:8080/uhyreutil/bmi
	@RequestMapping(value = "/bmi", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<BMIOutputDto> getCandidateBMI(@RequestBody BMIInputDto bmiInputData) {
		BMIOutputDto  bmiScore = null;
		try {
			LogUtil.LOG.info(
					"Uhyre getCandidateBMI, Request :\n" + UhyreUtil.getGsonInstance().toJson(bmiInputData));
			UhyreService uhyreService = new UhyreServiceImpl();
			bmiScore = uhyreService.getCandidateBMI(bmiInputData);
			if (bmiScore != null) {
				bmiScore.setStatus(ResponseUtil.SUCCESS);
				bmiScore.setMessage(ResponseUtil.SUCCESS);
			} else {
				bmiScore = new BMIOutputDto();
				bmiScore.setStatus(ResponseUtil.FAILURE_STATUS);
				bmiScore.setMessage(UhyreUtil.TECH_ERROR);
			}
		} catch (Exception ex) {
			LogUtil.LOG.warn(ex.getMessage());
			ex.printStackTrace();
			LogUtil.LOG.warn("Exception BAD_REQUEST - " + HttpStatus.BAD_REQUEST.value());
			bmiScore = new BMIOutputDto();
			bmiScore.setStatus(ResponseUtil.FAILURE_STATUS);
			bmiScore.setMessage(UhyreUtil.TECH_ERROR);
			return new ResponseEntity<BMIOutputDto>(bmiScore, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<BMIOutputDto>(bmiScore, HttpStatus.OK);
	}

}