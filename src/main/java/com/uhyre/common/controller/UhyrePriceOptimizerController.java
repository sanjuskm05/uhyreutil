package com.uhyre.common.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.uhyre.common.dto.BMIInputDto;
import com.uhyre.common.dto.BMIOutputDto;
import com.uhyre.common.dto.PriceOptInputDto;
import com.uhyre.common.dto.PriceOptOutputDto;
import com.uhyre.common.service.UhyreService;
import com.uhyre.common.service.UhyreServiceImpl;
import com.uhyre.common.util.LogUtil;
import com.uhyre.common.util.UhyreUtil;
import com.uhyre.common.util.ResponseUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

/**
 * 
 * @author Sanjay
 *
 */

@RestController
public class UhyrePriceOptimizerController {

	// Example http://localhost:8080/uhyreutil/bmi
	@RequestMapping(value = "/price/optmized", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<PriceOptOutputDto> getOptimumPrices(@RequestBody PriceOptInputDto priceOptInputData) {
		PriceOptOutputDto  priceOptOutputData = null;
		try {
			LogUtil.LOG.info(
					"Uhyre getSkillRelationships, Request :\n" + UhyreUtil.getGsonInstance().toJson(priceOptInputData));
			UhyreService uhyreService = new UhyreServiceImpl();
			priceOptOutputData = uhyreService.getOptimumPrices(priceOptInputData);
			if (priceOptOutputData == null) {
				priceOptOutputData = new PriceOptOutputDto();
				priceOptOutputData.setStatus(ResponseUtil.FAILURE_STATUS);
				priceOptOutputData.setMessage(UhyreUtil.TECH_ERROR);
			}
		} catch (Exception ex) {
			LogUtil.LOG.warn(ex.getMessage());
			ex.printStackTrace();
			LogUtil.LOG.warn("Exception BAD_REQUEST - " + HttpStatus.BAD_REQUEST.value());
			priceOptOutputData = new PriceOptOutputDto();
			priceOptOutputData.setStatus(ResponseUtil.FAILURE_STATUS);
			priceOptOutputData.setMessage(UhyreUtil.TECH_ERROR);
			return new ResponseEntity<PriceOptOutputDto>(priceOptOutputData, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<PriceOptOutputDto>(priceOptOutputData, HttpStatus.OK);
	}

}