package com.uhyre.common.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author Sanjay
 *
 */
public class PriceOptOutputDto implements Serializable{

	private static final long serialVersionUID = -1;
	
	private double candidateUhyreOptimizeRate = 0;	
	private double uhyreCommissionInDollars = 0;
	private double employerUhyreOptimizeRate = 0;
	private double employerBenefitInPercentage = 0;
	private double candidateBenefitOverMinInPercentage = 0;
	private double candidateBenefitOverAskingRateInPercentage = 0;
	private String status;
	private String message;
	
	public double getCandidateUhyreOptimizeRate() {
		return candidateUhyreOptimizeRate;
	}
	public void setCandidateUhyreOptimizeRate(double candidateUhyreOptimizeRate) {
		this.candidateUhyreOptimizeRate = candidateUhyreOptimizeRate;
	}
	public double getUhyreCommissionInDollars() {
		return uhyreCommissionInDollars;
	}
	public void setUhyreCommissionInDollars(double uhyreCommissionInDollars) {
		this.uhyreCommissionInDollars = uhyreCommissionInDollars;
	}
	public double getEmployerUhyreOptimizeRate() {
		return employerUhyreOptimizeRate;
	}
	public void setEmployerUhyreOptimizeRate(double employerUhyreOptimizeRate) {
		this.employerUhyreOptimizeRate = employerUhyreOptimizeRate;
	}
	public double getEmployerBenefitInPercentage() {
		return employerBenefitInPercentage;
	}
	public void setEmployerBenefitInPercentage(double employerBenefitInPercentage) {
		this.employerBenefitInPercentage = employerBenefitInPercentage;
	}
	public double getCandidateBenefitOverMinInPercentage() {
		return candidateBenefitOverMinInPercentage;
	}
	public void setCandidateBenefitOverMinInPercentage(double candidateBenefitOverMinInPercentage) {
		this.candidateBenefitOverMinInPercentage = candidateBenefitOverMinInPercentage;
	}
	public double getCandidateBenefitOverAskingRateInPercentage() {
		return candidateBenefitOverAskingRateInPercentage;
	}
	public void setCandidateBenefitOverAskingRateInPercentage(double candidateBenefitOverAskingRateInPercentage) {
		this.candidateBenefitOverAskingRateInPercentage = candidateBenefitOverAskingRateInPercentage;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
