package com.uhyre.common.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author Sanjay
 *
 */
public class BMIOutputDto implements Serializable{

	private static final long serialVersionUID = -1;
	
	private double candidateBMIScore = 0;
	private double candidateSkillMatcherScore = 0;
	private double candidateExpMatcherScore = 0;
	private double candidateEcoMatcherScore = 0;
	private double candidatePersonalityTraitMatcherScore = 0;
	private String status;
	private String message;
	
	
	public double getCandidateBMIScore() {
		return candidateBMIScore;
	}
	public void setCandidateBMIScore(double candidateBMIScore) {
		this.candidateBMIScore = candidateBMIScore;
	}
	public double getCandidateSkillMatcherScore() {
		return candidateSkillMatcherScore;
	}
	public void setCandidateSkillMatcherScore(double candidateSkillMatcherScore) {
		this.candidateSkillMatcherScore = candidateSkillMatcherScore;
	}
	public double getCandidateExpMatcherScore() {
		return candidateExpMatcherScore;
	}
	public void setCandidateExpMatcherScore(double candidateExpMatcherScore) {
		this.candidateExpMatcherScore = candidateExpMatcherScore;
	}
	public double getCandidateEcoMatcherScore() {
		return candidateEcoMatcherScore;
	}
	public void setCandidateEcoMatcherScore(double candidateEcoMatcherScore) {
		this.candidateEcoMatcherScore = candidateEcoMatcherScore;
	}
	public double getCandidatePersonalityTraitMatcherScore() {
		return candidatePersonalityTraitMatcherScore;
	}
	public void setCandidatePersonalityTraitMatcherScore(double candidatePersonalityTraitMatcherScore) {
		this.candidatePersonalityTraitMatcherScore = candidatePersonalityTraitMatcherScore;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
