package com.uhyre.common.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author Sanjay
 *
 */
public class SkillRelationDto implements Serializable{

	private static final long serialVersionUID = -1;
	
	private String skillName;
	private List<String> relatedSkills;
	private String status;
	private String message;
	
	public String getSkillName() {
		return skillName;
	}
	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}
	public List<String> getRelatedSkills() {
		return relatedSkills;
	}
	public void setRelatedSkills(List<String> relatedSkills) {
		this.relatedSkills = relatedSkills;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
