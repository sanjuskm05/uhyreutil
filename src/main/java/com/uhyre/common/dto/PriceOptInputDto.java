package com.uhyre.common.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author Sanjay
 *
 */
public class PriceOptInputDto implements Serializable{

	private static final long serialVersionUID = -1;
	
	private double employerActualRate = 0;
	private double candidateMinRate	= 0;
	private double candidateAskingRate = 0;
	private double uhyreCommissionInPercentage = 0;
	public double getEmployerActualRate() {
		return employerActualRate;
	}
	public void setEmployerActualRate(double employerActualRate) {
		this.employerActualRate = employerActualRate;
	}
	public double getCandidateMinRate() {
		return candidateMinRate;
	}
	public void setCandidateMinRate(double candidateMinRate) {
		this.candidateMinRate = candidateMinRate;
	}
	public double getCandidateAskingRate() {
		return candidateAskingRate;
	}
	public void setCandidateAskingRate(double candidateAskingRate) {
		this.candidateAskingRate = candidateAskingRate;
	}
	public double getUhyreCommissionInPercentage() {
		return uhyreCommissionInPercentage;
	}
	public void setUhyreCommissionInPercentage(double uhyreCommissionInPercentage) {
		this.uhyreCommissionInPercentage = uhyreCommissionInPercentage;
	}
	
	
}
