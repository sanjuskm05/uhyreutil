package com.uhyre.common.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.uhyre.common.pojo.CityInfo;

/**
 * 
 * @author Sanjay
 *
 */
public class CandidateEconomicDto implements Serializable{

	private static final long serialVersionUID = -1;
	
	private double candidateHourlyRate = 0;
	private Date candidateStartDate;
	private List<CityInfo> candidatePreferredLocations;
	private double uhyreHourlyCharges = 0;
	private Boolean isCandidateWillingToRelocate = Boolean.FALSE;
	private Boolean isCandidateWillingToTravel = Boolean.FALSE;
	private Boolean isCandidateWillingToPayForTravel = Boolean.FALSE;
	public double getCandidateHourlyRate() {
		return candidateHourlyRate;
	}
	public void setCandidateHourlyRate(double candidateHourlyRate) {
		this.candidateHourlyRate = candidateHourlyRate;
	}
	public Date getCandidateStartDate() {
		return candidateStartDate;
	}
	public void setCandidateStartDate(Date candidateStartDate) {
		this.candidateStartDate = candidateStartDate;
	}
	public List<CityInfo> getCandidatePreferredLocations() {
		return candidatePreferredLocations;
	}
	public void setCandidatePreferredLocations(List<CityInfo> candidatePreferredLocations) {
		this.candidatePreferredLocations = candidatePreferredLocations;
	}
	public double getUhyreHourlyCharges() {
		return uhyreHourlyCharges;
	}
	public void setUhyreHourlyCharges(double uhyreHourlyCharges) {
		this.uhyreHourlyCharges = uhyreHourlyCharges;
	}
	public Boolean isCandidateWillingToTravel() {
		return isCandidateWillingToTravel;
	}
	public void setIsCandidateWillingToTravel(Boolean isCandidateWillingToTravel) {
		this.isCandidateWillingToTravel = isCandidateWillingToTravel;
	}
	public Boolean isCandidateWillingToPayForTravel() {
		return isCandidateWillingToPayForTravel;
	}
	public void setIsCandidateWillingToPayForTravel(Boolean isCandidateWillingToPayForTravel) {
		this.isCandidateWillingToPayForTravel = isCandidateWillingToPayForTravel;
	}
	public Boolean isCandidateWillingToRelocate() {
		return isCandidateWillingToRelocate;
	}
	public void setIsCandidateWillingToRelocate(Boolean isCandidateWillingToRelocate) {
		this.isCandidateWillingToRelocate = isCandidateWillingToRelocate;
	}
	
	
}
