package com.uhyre.common.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author Sanjay
 *
 */
public class CandidateWorkAuthDto implements Serializable{

	private static final long serialVersionUID = -1;
	
	private Boolean isEligibleOnVISAEmployerRequirement ;
	private Boolean isEligibleOnVISALegalWorker;
	private Boolean isEligibleOnCitizenOnlyEmployerRequirement;
	private Boolean isEligibleCitizen;
	private Boolean isEligibleCitizenOrPermanentResidentOnlyEmployerRequirement ;
	private Boolean isEligibleCitizenOrPermanentResident;
	
	public Boolean getIsEligibleOnVISAEmployerRequirement() {
		return isEligibleOnVISAEmployerRequirement;
	}
	public void setIsEligibleOnVISAEmployerRequirement(Boolean isEligibleOnVISAEmployerRequirement) {
		this.isEligibleOnVISAEmployerRequirement = isEligibleOnVISAEmployerRequirement;
	}
	public Boolean getIsEligibleOnVISALegalWorker() {
		return isEligibleOnVISALegalWorker;
	}
	public void setIsEligibleOnVISALegalWorker(Boolean isEligibleOnVISALegalWorker) {
		this.isEligibleOnVISALegalWorker = isEligibleOnVISALegalWorker;
	}
	public Boolean getIsEligibleOnCitizenOnlyEmployerRequirement() {
		return isEligibleOnCitizenOnlyEmployerRequirement;
	}
	public void setIsEligibleOnCitizenOnlyEmployerRequirement(Boolean isEligibleOnCitizenOnlyEmployerRequirement) {
		this.isEligibleOnCitizenOnlyEmployerRequirement = isEligibleOnCitizenOnlyEmployerRequirement;
	}
	public Boolean getIsEligibleCitizen() {
		return isEligibleCitizen;
	}
	public void setIsEligibleCitizen(Boolean isEligibleCitizen) {
		this.isEligibleCitizen = isEligibleCitizen;
	}
	public Boolean getIsEligibleCitizenOrPermanentResidentOnlyEmployerRequirement() {
		return isEligibleCitizenOrPermanentResidentOnlyEmployerRequirement;
	}
	public void setIsEligibleCitizenOrPermanentResidentOnlyEmployerRequirement(
			Boolean isEligibleCitizenOrPermanentResidentOnlyEmployerRequirement) {
		this.isEligibleCitizenOrPermanentResidentOnlyEmployerRequirement = isEligibleCitizenOrPermanentResidentOnlyEmployerRequirement;
	}
	public Boolean getIsEligibleCitizenOrPermanentResident() {
		return isEligibleCitizenOrPermanentResident;
	}
	public void setIsEligibleCitizenOrPermanentResident(Boolean isEligibleCitizenOrPermanentResident) {
		this.isEligibleCitizenOrPermanentResident = isEligibleCitizenOrPermanentResident;
	}
	
	
}
