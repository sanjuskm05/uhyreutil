package com.uhyre.common.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.uhyre.common.pojo.CityInfo;

/**
 * 
 * @author Sanjay
 *
 */
public class EmployerEconomicDto implements Serializable{

	private static final long serialVersionUID = -1;
	
	private double employerHourlyRate = 0;
	private Date employerStartDate;
	private CityInfo employerLocation;
	
	public double getEmployerHourlyRate() {
		return employerHourlyRate;
	}
	public void setEmployerHourlyRate(double employerHourlyRate) {
		this.employerHourlyRate = employerHourlyRate;
	}
	public Date getEmployerStartDate() {
		return employerStartDate;
	}
	public void setEmployerStartDate(Date employerStartDate) {
		this.employerStartDate = employerStartDate;
	}
	public CityInfo getEmployerLocation() {
		return employerLocation;
	}
	public void setEmployerLocation(CityInfo employerLocation) {
		this.employerLocation = employerLocation;
	}
		
}
