package com.uhyre.common.dto;

import java.io.Serializable;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 
 * @author Sanjay
 *
 */
public class SkillListDto implements Serializable{

	private static final long serialVersionUID = -1;
	
	private Set<String> skills;

	public Set<String> getSkills() {
		return skills.stream()
		        .map(String::toLowerCase)
		        .collect(Collectors.toSet());
	}

	public void setSkills(Set<String> skills) {
		this.skills = skills.stream()
		        .map(String::toLowerCase)
		        .collect(Collectors.toSet());
	}	
	
}
