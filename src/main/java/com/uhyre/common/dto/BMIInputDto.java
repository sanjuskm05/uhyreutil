package com.uhyre.common.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author Sanjay
 *
 */
public class BMIInputDto implements Serializable{

	private static final long serialVersionUID = -1;
	
	private SkillListDto employerMandatorySkills;
	private SkillListDto employerNiceToHaveSkills;
	private SkillListDto candidateSkills;
	private CandidateWorkAuthDto candidateWorkAuthData;
	private int candidateRelevantExperienceInYears = 0;
	private int employerMandatoryMinExperienceInYears = 0;
	private int employerMandatoryMaxExperienceInYears = 0;
	private EmployerEconomicDto employerEcoData;
	private CandidateEconomicDto candidateEcoData;
	private PersonalityTraitsDto candidatePersonalityTraits;
	private PersonalityTraitsDto employerRequirePersonalityTraits;
	
	public SkillListDto getEmployerMandatorySkills() {
		return employerMandatorySkills;
	}
	public void setEmployerMandatorySkills(SkillListDto employerMandatorySkills) {
		this.employerMandatorySkills = employerMandatorySkills;
	}
	public SkillListDto getEmployerNiceToHaveSkills() {
		return employerNiceToHaveSkills;
	}
	public void setEmployerNiceToHaveSkills(SkillListDto employerNiceToHaveSkills) {
		this.employerNiceToHaveSkills = employerNiceToHaveSkills;
	}
	public SkillListDto getCandidateSkills() {
		return candidateSkills;
	}
	public void setCandidateSkills(SkillListDto candidateSkills) {
		this.candidateSkills = candidateSkills;
	}
	public CandidateWorkAuthDto getCandidateWorkAuthData() {
		return candidateWorkAuthData;
	}
	public void setCandidateWorkAuthData(CandidateWorkAuthDto candidateWorkAuthData) {
		this.candidateWorkAuthData = candidateWorkAuthData;
	}
	public int getCandidateRelevantExperienceInYears() {
		return candidateRelevantExperienceInYears;
	}
	public void setCandidateRelevantExperienceInYears(int candidateRelevantExperienceInYears) {
		this.candidateRelevantExperienceInYears = candidateRelevantExperienceInYears;
	}
	public int getEmployerMandatoryMinExperienceInYears() {
		return employerMandatoryMinExperienceInYears;
	}
	public void setEmployerMandatoryMinExperienceInYears(int employerMandatoryMinExperienceInYears) {
		this.employerMandatoryMinExperienceInYears = employerMandatoryMinExperienceInYears;
	}
	public int getEmployerMandatoryMaxExperienceInYears() {
		return employerMandatoryMaxExperienceInYears;
	}
	public void setEmployerMandatoryMaxExperienceInYears(int employerMandatoryMaxExperienceInYears) {
		this.employerMandatoryMaxExperienceInYears = employerMandatoryMaxExperienceInYears;
	}
	public EmployerEconomicDto getEmployerEcoData() {
		return employerEcoData;
	}
	public void setEmployerEcoData(EmployerEconomicDto employerEcoData) {
		this.employerEcoData = employerEcoData;
	}
	public CandidateEconomicDto getCandidateEcoData() {
		return candidateEcoData;
	}
	public void setCandidateEcoData(CandidateEconomicDto candidateEcoData) {
		this.candidateEcoData = candidateEcoData;
	}
	public PersonalityTraitsDto getCandidatePersonalityTraits() {
		return candidatePersonalityTraits;
	}
	public void setCandidatePersonalityTraits(PersonalityTraitsDto candidatePersonalityTraits) {
		this.candidatePersonalityTraits = candidatePersonalityTraits;
	}
	public PersonalityTraitsDto getEmployerRequirePersonalityTraits() {
		return employerRequirePersonalityTraits;
	}
	public void setEmployerRequirePersonalityTraits(PersonalityTraitsDto employerRequirePersonalityTraits) {
		this.employerRequirePersonalityTraits = employerRequirePersonalityTraits;
	}	
}
