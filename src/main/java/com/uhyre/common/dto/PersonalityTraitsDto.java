package com.uhyre.common.dto;

import java.io.Serializable;
import java.util.Set;

/**
 * 
 * @author Sanjay
 *
 */
public class PersonalityTraitsDto implements Serializable{

	private static final long serialVersionUID = -1;
	
	private int extrovertPercentage;
	private int introvertPercentage;
	private int intuitivePercentage;
	private int observantPercentage;
	private int thinkingPercentage;
	private int feelingPercentage;
	private int judgingPercentage;
	private int prospectingPercentage;
	public int getExtrovertPercentage() {
		return extrovertPercentage;
	}
	public void setExtrovertPercentage(int extrovertPercentage) {
		this.extrovertPercentage = extrovertPercentage;
	}
	public int getIntrovertPercentage() {
		return introvertPercentage;
	}
	public void setIntrovertPercentage(int introvertPercentage) {
		this.introvertPercentage = introvertPercentage;
	}
	public int getIntuitivePercentage() {
		return intuitivePercentage;
	}
	public void setIntuitivePercentage(int intuitivePercentage) {
		this.intuitivePercentage = intuitivePercentage;
	}
	public int getObservantPercentage() {
		return observantPercentage;
	}
	public void setObservantPercentage(int observantPercentage) {
		this.observantPercentage = observantPercentage;
	}
	public int getThinkingPercentage() {
		return thinkingPercentage;
	}
	public void setThinkingPercentage(int thinkingPercentage) {
		this.thinkingPercentage = thinkingPercentage;
	}
	public int getFeelingPercentage() {
		return feelingPercentage;
	}
	public void setFeelingPercentage(int feelingPercentage) {
		this.feelingPercentage = feelingPercentage;
	}
	public int getJudgingPercentage() {
		return judgingPercentage;
	}
	public void setJudgingPercentage(int judgingPercentage) {
		this.judgingPercentage = judgingPercentage;
	}
	public int getProspectingPercentage() {
		return prospectingPercentage;
	}
	public void setProspectingPercentage(int prospectingPercentage) {
		this.prospectingPercentage = prospectingPercentage;
	}
	
	
	
}
