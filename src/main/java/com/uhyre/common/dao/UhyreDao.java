package com.uhyre.common.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import javax.sql.DataSource;

import com.uhyre.common.pojo.CityInfo;

/**
 * 
 * @author Sanjay
 *
 */
public interface UhyreDao{

	  public DataSource getDataSource();
	  
	  public void setDataSource(DataSource dataSource);
	  
	  public ArrayList<String> getRelatedSkills(String skill) throws SQLException;
	  public CityInfo getCityInformation(String name, String stateCode, String zipcode) throws SQLException;
}