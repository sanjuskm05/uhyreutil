package com.uhyre.common.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.sql.DataSource;

import com.uhyre.common.pojo.CityInfo;
import com.uhyre.common.util.UhyreUtil;

/**
 * 
 * @author Sanjay
 *
 */
public class UhyreDaoImpl implements UhyreDao {
	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public DataSource getDataSource() {
		return this.dataSource;
	}

	private static final String GET_RELATED_SKILLS = " select distinct tar_sc.skill "
			+ " from skill_links sl, skills_with_coordinates src_sc, skills_with_coordinates tar_sc " 
			+ " where "
			+ " upper(src_sc.skill) like upper(?) "
			+ " and (src_sc.indexId = sl.sourceId or tar_sc.indexId = sl.sourceId) "
			+ " and (tar_sc.indexId = sl.targetId or src_sc.indexId = sl.targetId) " 
			+ " order by src_sc.indexId ";

	/**
	 * fetch the related skills when given a skill name
	 * 
	 * @param skill
	 *            String
	 * @return ArrayList<String>
	 */
	@Override
	public ArrayList<String> getRelatedSkills(String skill) throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<String> relatedSkills = new ArrayList<String>(20);
		try {
			conn = this.dataSource.getConnection();

			ps = conn.prepareStatement(GET_RELATED_SKILLS);
			int parameterIndex = 1;
			ps.setString(parameterIndex++, "%" + skill + "%");
			rs = ps.executeQuery();
			while (rs.next()) {
				relatedSkills.add(rs.getString("skill"));
			}
		} finally {
			UhyreUtil.cleanUpSQLData(ps, rs, conn, this);
		}
		return relatedSkills;
	}

	private static final String GET_CITY_INFO = " select city, state, zipcode, latitude, longitude " + 
												" from usa_cities_zipcode" + 
												" where ( upper(city) like upper(?) " +
												" and upper(state) like upper(?) ) " +
												" or zipcode = ? ";
	/**
	 * fetch the city information when given a city name or zipcode
	 * 
	 * @param name
	 *            String
	 * @param stateCode
	 *            String
	 * @param zipcode
	 *            String            
	 * @return CityInfo
	 */
	@Override
	public CityInfo getCityInformation(String name, String stateCode, String zipcode) throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		CityInfo cityInfo = new CityInfo();
		try {
			conn = this.dataSource.getConnection();

			ps = conn.prepareStatement(GET_CITY_INFO);
			int parameterIndex = 1;
			ps.setString(parameterIndex++, name);
			ps.setString(parameterIndex++, stateCode);
			ps.setString(parameterIndex++, zipcode);
			rs = ps.executeQuery();
			if(rs.next()) {
				cityInfo.setName(rs.getString("city"));
				cityInfo.setZipcode(rs.getString("zipcode"));
				cityInfo.setStateCode(rs.getString("state"));
				cityInfo.setLatitude(rs.getDouble("latitude"));
				cityInfo.setLongitude(rs.getDouble("longitude"));
			}
		} finally {
			UhyreUtil.cleanUpSQLData(ps, rs, conn, this);
		}
		return cityInfo;
	}
}
