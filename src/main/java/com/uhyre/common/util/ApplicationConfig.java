package com.uhyre.common.util;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author Sanjay
 *
 */
public class ApplicationConfig implements Serializable{

	private static final long serialVersionUID = -1;
	
	private double bmiSkillWeight;
	private double bmiExperienceWeight;
	private double bmiPersionalityWeight;
	private double earthRadiusInKM;
	private double earthRadiusInMiles;
	private double cityLimitsInMiles;
	
	public double getBmiSkillWeight() {
		return bmiSkillWeight;
	}
	public void setBmiSkillWeight(double bmiSkillWeight) {
		this.bmiSkillWeight = bmiSkillWeight;
	}
	public double getBmiExperienceWeight() {
		return bmiExperienceWeight;
	}
	public void setBmiExperienceWeight(double bmiExperienceWeight) {
		this.bmiExperienceWeight = bmiExperienceWeight;
	}
	public double getBmiPersionalityWeight() {
		return bmiPersionalityWeight;
	}
	public void setBmiPersionalityWeight(double bmiPersionalityWeight) {
		this.bmiPersionalityWeight = bmiPersionalityWeight;
	}
	public double getEarthRadiusInKM() {
		return earthRadiusInKM;
	}
	public void setEarthRadiusInKM(double earthRadiusInKM) {
		this.earthRadiusInKM = earthRadiusInKM;
	}
	public double getEarthRadiusInMiles() {
		return earthRadiusInMiles;
	}
	public void setEarthRadiusInMiles(double earthRadiusInMiles) {
		this.earthRadiusInMiles = earthRadiusInMiles;
	}
	public double getCityLimitsInMiles() {
		return cityLimitsInMiles;
	}
	public void setCityLimitsInMiles(double cityLimitsInMiles) {
		this.cityLimitsInMiles = cityLimitsInMiles;
	}	
	
}