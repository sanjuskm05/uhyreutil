package com.uhyre.common.util;

public enum DistanceType {
	Miles,
    KiloMeters
}
