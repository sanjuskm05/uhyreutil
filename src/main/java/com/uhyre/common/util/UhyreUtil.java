package com.uhyre.common.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.client.RestTemplate;
import com.google.gson.Gson;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.NumberParseException.ErrorType;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.uhyre.common.dao.UhyreDao;
import com.uhyre.common.dto.CandidateEconomicDto;
import com.uhyre.common.dto.EmployerEconomicDto;
import com.uhyre.common.dto.PersonalityTraitsDto;
import com.uhyre.common.dto.SkillListDto;
import com.uhyre.common.pojo.CityInfo;

/**
 * 
 * @author Sanjay
 *
 */
public class UhyreUtil{

	public static String TECH_ERROR = "Sorry, an unexpected error has occurred. "
			+ "Please write to admin@u-hyre.com to report this.";
	//Constants
	private static ApplicationContext context = null;	
	private static Gson gson = null;
	private static RestTemplate restTemplate = null;
	
	public static double BMI_SKILL_WEIGHT;
	public static double BMI_EXPERIENCE_WEIGHT;
	public static double BMI_PERSIONALITY_WEIGHT;
	public static double EARTH_RADIUS_IN_KM;
	public static double EARTH_RADIUS_IN_MILES;
	public static double CITY_LIMITS_IN_MILES;
	
	static{
		ApplicationContext appContext = getApplicationContext();
	    ApplicationConfig config = (ApplicationConfig)appContext.getBean("appConfig");
	    BMI_SKILL_WEIGHT = config.getBmiSkillWeight();
	    BMI_EXPERIENCE_WEIGHT = config.getBmiExperienceWeight();
	    BMI_PERSIONALITY_WEIGHT = config.getBmiPersionalityWeight();
	    EARTH_RADIUS_IN_KM = config.getEarthRadiusInKM();
	    EARTH_RADIUS_IN_MILES = config.getEarthRadiusInMiles();
	    CITY_LIMITS_IN_MILES = config.getCityLimitsInMiles();
	}
	public static ApplicationContext getApplicationContext(){
		if(context == null){
			context = new ClassPathXmlApplicationContext("spring-config.xml");
		}
		return context;
	}
	/**
	 * 
	 * @param statement
	 * @param rs
	 * @param conn
	 * @param caller
	 */
	public static void cleanUpSQLData(Statement statement, ResultSet rs, Connection conn, Object caller) {

        //Take care of cleaning up the sql objects!
        try {
            if (rs != null) {
                rs.close();
            }
        }
        catch (Exception sqe) {
            sqe.printStackTrace();
        }
        try {
            if (statement != null) {
                statement.close();
            }
        }
        catch (Exception sqe) {
        	sqe.printStackTrace();
        }
        
        try {
            if (conn != null) {
                conn.close();
            }
        }
        catch (Exception sqe) {
        	sqe.printStackTrace();
        }
    }
	/**
	 * 
	 * @param statement
	 * @param rs
	 * @param caller
	 */
	public static void cleanUpSQLData(Statement statement, ResultSet rs,  Object caller) {

        //Take care of cleaning up the sql objects!
        try {
            if (rs != null) {
                rs.close();
            }
        }
        catch (Exception sqe) {
            sqe.printStackTrace();
        }
        try {
            if (statement != null) {
                statement.close();
            }
        }
        catch (Exception sqe) {
        	sqe.printStackTrace();
        }

    }

	/**
	 * 
	 * @param contactNumber
	 * @return
	 */
	
	public static String getContactNumber(String contactNumber){
		return UhyreUtil.getContactNumber(contactNumber,"US");
	}
	/**
	 * 
	 * @param contactNumber
	 * @return
	 */
	public static String getContactNumberRegion(String contactNumber){
		String regionCode = ""; 
		PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
		PhoneNumber number ;
		 try {
		        number = phoneNumberUtil.parse(contactNumber, null);
		        regionCode = phoneNumberUtil.getRegionCodeForNumber(number);
		 }catch (NumberParseException e) {
		    	//e.getStackTrace();		    	
		 }finally{
			 if(regionCode == null || regionCode.isEmpty()){
				 regionCode = "US";// by default, it will take USA international code
			 }
		 }
		 return regionCode;
	}
	/**
	 * 
	 * @param contactNumber
	 * @param region
	 * @return
	 */
	public static String getContactNumber(String contactNumber, String region){

		PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
		String decodedNumber = null;
		PhoneNumber number;
		    try {
		        number = phoneNumberUtil.parse(contactNumber, region);// by default, it will take USA international code
		        decodedNumber = phoneNumberUtil.format(number, PhoneNumberFormat.E164);
		    } catch (NumberParseException e) {
		    	//e.getStackTrace();
		    	if(e.getErrorType() == ErrorType.INVALID_COUNTRY_CODE){
		    		decodedNumber = "1" + contactNumber;// Just to make sure it takes USA international code
		    	}
		    	else if(e.getErrorType() == ErrorType.NOT_A_NUMBER ){
		    		return "";
		    	}
		    	else if(e.getErrorType() == ErrorType.TOO_LONG || 
		    			e.getErrorType() == ErrorType.TOO_SHORT_AFTER_IDD || 
		    			e.getErrorType() == ErrorType.TOO_SHORT_NSN){
		    		decodedNumber = contactNumber;
		    	}
		    }
		    decodedNumber = decodedNumber.replaceAll("\\D+",""); //first remove if it there and then add '+'
		    decodedNumber = "+" + decodedNumber;
		return decodedNumber;
	}
	

	/**
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNumber(String str){
		return NumberUtils.isNumber(str);
	}
	
	/**
	 * Removes diacritics (~= accents) from a string. The case will not be altered.
	   For instance, '�' will be replaced by 'a'.
	 * @param str
	 * @return
	 */
	public static String stripAccents(String str){
		return StringUtils.stripAccents(str);
	}

	/**
	 * Removes the duplicate in the array and join them back
	 * @param items
	 * @return String
	 */
	public static String removeDuplicates(String[] items){
		List<String> list = Arrays.asList(items);
		return StringUtils.join(removeDuplicates(list).iterator(),",");
	}
	
	/**
	 *  Removes the duplicate in a list
	 * @param list
	 * @return set
	 */
	public static Set<String> removeDuplicates(List<String> list) {

		// Record encountered Strings in HashSet.
		Set<String> set = new HashSet<String>();

		// Loop over argument list.
		for (String item : list) {
		    // If String is not in set, add it to the set.
		    if (!set.contains(item.trim())) {
		    	set.add(item.trim());
		    }
		}
		return set;
	}

	/**
	 * 
	 * @return Gson
	 */
	public static Gson getGsonInstance(){
		 if(gson == null){
			 gson = new Gson();
		 }
		 return gson;
	}

	/**
	 * 
	 * @return RestTemplate
	 */
	public static RestTemplate getRestTemplateInstance(){
		  if(restTemplate == null){
			  restTemplate = new RestTemplate();
		  }
		  return restTemplate;
	}
	 /**
	  * get candidate skill matcher score
	  */
	public static double getCandidateSkillMatcherScore(Set<String> employerMandatorySkills, 
			Set<String> employerNiceToHaveSkills,
			Set<String> candidateSkills  ){
		double score = 0;
		double mandatorySkillCount = 0;
		double niceToHaveSkillCount = 0;
		double additionalSkillCount = 0;
		
		for(String skill : candidateSkills){
			if(employerMandatorySkills.contains(skill)){
				mandatorySkillCount++;
			}else if(employerNiceToHaveSkills.contains(skill)){
				niceToHaveSkillCount++;
			}else{
				additionalSkillCount++;
			}
		}
		
		score = mandatorySkillCount * Math.pow(UhyreUtil.BMI_SKILL_WEIGHT,3) +
				niceToHaveSkillCount * ( 3 * Math.pow(UhyreUtil.BMI_SKILL_WEIGHT,2)) +
				additionalSkillCount * ( 2 * UhyreUtil.BMI_SKILL_WEIGHT);
		return score;
	}
	
	 /**
	  * get employer skill matcher score
	  */
	public static double getEmployerSkillMatcherScore(Set<String> employerMandatorySkills, 
			Set<String> employerNiceToHaveSkills  ){
		double score = 0;

		score = employerMandatorySkills.size() * Math.pow(UhyreUtil.BMI_SKILL_WEIGHT,3) +
				employerNiceToHaveSkills.size() * ( 3 * Math.pow(UhyreUtil.BMI_SKILL_WEIGHT,2)) ;
		return score;
	}
	
	 /**
	  * get candidate experience matcher score
	  */
	public static double getCandidateExpMatcherScore(int candidateRelevantExperienceInYears, 
			int employerMandatoryMinExperienceInYears,
			int employerMandatoryMaxExperienceInYears){
		double score = 0;
		double multiplyer = 1;
		if(employerMandatoryMinExperienceInYears <= candidateRelevantExperienceInYears && 
				employerMandatoryMaxExperienceInYears >= candidateRelevantExperienceInYears){
			multiplyer = UhyreUtil.BMI_EXPERIENCE_WEIGHT;
		}else if(candidateRelevantExperienceInYears > 10){
			multiplyer = 4;//TODO declare them as constants
		}else if(candidateRelevantExperienceInYears >=8 && candidateRelevantExperienceInYears <= 10){
			multiplyer = 3;
		}else if(candidateRelevantExperienceInYears >=4 && candidateRelevantExperienceInYears <= 7){
			multiplyer = 2;
		}else if(candidateRelevantExperienceInYears >=0 && candidateRelevantExperienceInYears <=3){
			multiplyer = 1;
		}
		score = multiplyer * UhyreUtil.BMI_EXPERIENCE_WEIGHT;
		
		return score;
	}
	 /**
	  * get employer experience matcher score
	  */
	public static double getEmployerExpMatcherScore(){
		double score = 0;
		score = Math.pow(UhyreUtil.BMI_EXPERIENCE_WEIGHT,2);
		return score;
	}
	 /**
	  * get candidate personality matcher score
	  */
	public static double getCandidatePersonalityTraitMatcherScore(PersonalityTraitsDto candidateTraits,
			PersonalityTraitsDto employerRequiredTraits){
		double score = 0;
		
		if(candidateTraits.getExtrovertPercentage() >= employerRequiredTraits.getExtrovertPercentage()){
			score+=candidateTraits.getExtrovertPercentage();
		}
		if(candidateTraits.getFeelingPercentage() >= employerRequiredTraits.getFeelingPercentage()){
			score+=candidateTraits.getFeelingPercentage();
		}
		if(candidateTraits.getIntrovertPercentage() >= employerRequiredTraits.getIntrovertPercentage()){
			score+=candidateTraits.getIntrovertPercentage();
		}
		if(candidateTraits.getIntuitivePercentage() >= employerRequiredTraits.getIntuitivePercentage()){
			score+=candidateTraits.getIntuitivePercentage();
		}
		
		if(candidateTraits.getJudgingPercentage() >= employerRequiredTraits.getJudgingPercentage()){
			score+=candidateTraits.getJudgingPercentage();
		}
		if(candidateTraits.getObservantPercentage() >= employerRequiredTraits.getObservantPercentage()){
			score+=candidateTraits.getObservantPercentage();
		}
		if(candidateTraits.getProspectingPercentage() >= employerRequiredTraits.getProspectingPercentage()){
			score+=candidateTraits.getProspectingPercentage();
		}
		if(candidateTraits.getThinkingPercentage() >= employerRequiredTraits.getThinkingPercentage()){
			score+=candidateTraits.getThinkingPercentage();
		}

		return score;
	}
	
	/**
	 * get ecomonic score
	 * @return economic score
	 * @throws SQLException 
	 * 
	 */
	public static double getCandidateEconomicScore(CandidateEconomicDto candEcoDto, 
			EmployerEconomicDto empEcoDto) throws SQLException{
		double score = 0;
		if(candEcoDto.getCandidateHourlyRate() < (empEcoDto.getEmployerHourlyRate() - candEcoDto.getUhyreHourlyCharges())){
			score+=45;//TODO change to constants
		}else{
			score+=10;
		}
		
		if(candEcoDto.getCandidateStartDate().before(empEcoDto.getEmployerStartDate())){
			score+=25;
		}else{
			score+=10;
		}
		UhyreDao uhyreDao = (UhyreDao) UhyreUtil.getApplicationContext().getBean("uhyreDAO");
		List<CityInfo> candidateCityInfos =  new ArrayList<CityInfo>(10);
		for(CityInfo candidatePreferredCity : candEcoDto.getCandidatePreferredLocations()){
			candidatePreferredCity = uhyreDao.getCityInformation(candidatePreferredCity.getName(),
					candidatePreferredCity.getStateCode(), 
					candidatePreferredCity.getZipcode());
			candidateCityInfos.add(candidatePreferredCity);
		}
		
		CityInfo employerCityInfo = uhyreDao.getCityInformation(empEcoDto.getEmployerLocation().getName(), 
							empEcoDto.getEmployerLocation().getStateCode(),
							empEcoDto.getEmployerLocation().getZipcode());
		
		
		if(candEcoDto.isCandidateWillingToPayForTravel() && candEcoDto.isCandidateWillingToTravel()){

			if(isAnyPreferredCityNearer(candidateCityInfos, employerCityInfo)){
				score+=30;
			}else if(candEcoDto.isCandidateWillingToRelocate()){
				score+=30;
			}else{
				score+=20;
			}
		}else{
			if(isAnyPreferredCityNearer(candidateCityInfos, employerCityInfo)){
				score+=30;
			}else if(candEcoDto.isCandidateWillingToRelocate()){
				score+=20;
			}else{
				score+=10;
			}
		}
		return score;
	}
	
	/**
	 * Calculate the distance between two co-ordinates on earth in miles/km
	 * 
	 * @param srcLatitude
	 * @param srcLongitude
	 * @param destLatitude
	 * @param destLongitude
	 * @param distType
	 * @return
	 */
	
	public static double getLocationDistance(double srcLatitude, double srcLongitude, 
			double destLatitude, double destLongitude, 
			DistanceType distType) {
	    double dLat = Math.toRadians(destLatitude - srcLatitude);
	    double dLon = Math.toRadians(destLongitude - srcLongitude);
	    double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
			       Math.cos(Math.toRadians(srcLatitude)) * Math.cos(Math.toRadians(destLatitude)) 
			      * Math.sin(dLon / 2) * Math.sin(dLon / 2);
	    double angle = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	    double distanceInKM = (distType.equals(DistanceType.KiloMeters) ? UhyreUtil.EARTH_RADIUS_IN_KM : UhyreUtil.EARTH_RADIUS_IN_MILES)
	    					  * angle;
	    return distanceInKM;
	}
	
	private static boolean isAnyPreferredCityNearer(List<CityInfo> candidatePreferred, CityInfo empCityInfo) {
		for(CityInfo candidateCityInfo : candidatePreferred) {
			double distance = UhyreUtil.getLocationDistance(candidateCityInfo.getLatitude(), candidateCityInfo.getLongitude(), 
					empCityInfo.getLatitude(), empCityInfo.getLongitude(), DistanceType.Miles);
			if(distance < UhyreUtil.CITY_LIMITS_IN_MILES) {
				return true;
			}
		}
		
		return false;
	}
	/**
	 * 
	 * @param a
	 * @return
	 */
	public static double getAverage(double... a ) {
		double sum = 0;
		for(double i : a) {
			sum+=i;
		}
		if(a.length > 0) {
			return sum/a.length;
		}else {
			return sum;
		}
	}
}