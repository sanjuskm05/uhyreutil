package com.uhyre.common.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * 
 * @author Sanjay
 *
 */
public abstract class DateUtility {
	/**
	 * Represents a hyphen-delimted date, of the form "MM-dd-yyyy".
	 * 
	 * @see java.text.SimpleDateFormat
	 **/
	public static final String HYPENATED_DATE = "MM-dd-yyyy";

	/**
	 * Represents a slash-delimited date, of the form "MM/dd/yyyy".
	 * 
	 * @see java.text.SimpleDateFormat
	 **/
	public static final String SLASHED_DATE = "MM/dd/yyyy";

	/**
	 * Represents a slash-delimited date with time component, of the form
	 * "MM/dd/yyyy hh:mm a".
	 * 
	 * @see java.text.SimpleDateFormat
	 **/
	public static final String SLASHED_DATE_WITH_TIME = "MM/dd/yyyy hh:mm a";

	/**
	 * Represents a slash-delimited date with time component with seconds, of
	 * the form "MM/dd/yyyy hh:mm:ss a".
	 * 
	 * @see java.text.SimpleDateFormat
	 **/
	public static final String SLASHED_DATE_WITH_TIME_SECONDS = "MM/dd/yyyy hh.mm.ss a";

	/**
	 * Represents a slash-delimited date with time component with timezone, of
	 * the form "MM/dd/yyyy hh:mm a z".
	 * 
	 * @see java.text.SimpleDateFormat
	 **/
	public static final String SLASHED_DATE_WITH_TIME_TIMEZONE = "MM/dd/yyyy hh:mm a z";

	/**
	 * Represents a slash-delimited date with time component with seconds and
	 * timezone, of the form "MM/dd/yyyy hh:mm:ss a z".
	 * 
	 * @see java.text.SimpleDateFormat
	 **/
	public static final String SLASHED_DATE_WITH_TIME_SECONDS_TIMEZONE = "MM/dd/yyyy hh.mm.ss a z";

	/**
	 * Represents a positional date, of the form ddMMyyyy".
	 * 
	 * @see java.text.SimpleDateFormat
	 **/
	public static final String POSITIONAL_DATE = "ddMMyyyy";

	public static final String POSITIONAL_DATE_WITH_TIME = DateUtility.POSITIONAL_DATE + "HHmmss";

	/**
	 * Represents a slash-delimited date, of the form "dd/MM/yyyy".
	 * 
	 * @see java.text.SimpleDateFormat
	 **/
	public static final String SLASHED_DATE_CANADA = "dd/MM/yyyy";

	/**
	 * Represents a slash-delimited date with time component, of the form
	 * "dd/MM/yyyy hh:mm a".
	 * 
	 * @see java.text.SimpleDateFormat
	 **/
	public static final String SLASHED_DATE_CANADA_WITH_TIME = "dd/MM/yyyy hh:mm a";

	/**
	 * Represents the standard (non-24-hour) time, in the form "hh:mm a".
	 * 
	 * @see java.text.SimpleDateFormat
	 **/
	public static final String STANDARD_TIME = "h:mm a";

	/**
	 * Should be used in dateToString(Date, String, String) as the third
	 * argument, rather than every screen instantiating its own blank string.
	 * 
	 * @see #dateToString(Date, String, String);
	 **/
	public static final String BLANK_STRING = "";

	public static final String ISO_CCYYMMDD_FORMAT = "yyyyMMdd";

	public static final String ISO_YYMMDD_FORMAT = "yyMMdd";

	public static final String TIME_24HR_FORMAT = "kkmm";

	public final static String CARD_EXPIRE_DATE_FORMAT = "MM/yy";

	/**
	 * Used by AS2 to convert time passed from HTML text Timestamp and vice
	 * versa
	 */
	public static final String AS2_TIMESTAMP_FORMAT = "EEE, dd MMM yyyy HH:mm:ss z";

	/**
	 * Used, synchronized, by several methods to perform computations. YOU MUST
	 * SYNCHRONIZE THIS VARIABLE IF YOU USE IT.
	 **/
	private static final GregorianCalendar _calendar = new GregorianCalendar();

	static {
		_calendar.setLenient(false);
	}

	/**
	 * Used, synchronized, by several methods to perform computations.
	 **/
	private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat();

	static {
		simpleDateFormat.setLenient(false);
	}

	/**
	 * Constant number of milliseconds in a day
	 **/
	public static final int MILLIS_PER_DAY = 86400000;

	/**
	 * Constant number of milliseconds in an hour
	 **/
	public static final int MILLIS_PER_HOUR = 3600000;

	/**
	 * Constant number of milliseconds in a minute
	 **/
	public static final int MILLIS_PER_MINUTE = 60000;

	/**
	 * @return The current date and time, precise to the nearest millisecond.
	 **/
	public static Timestamp getCurrentTimestamp() {
		return new Timestamp(System.currentTimeMillis());
	}

	private static java.sql.Date alternativeGetDate(java.util.Date date, String format) {
		String strDate = dateToString(date, format);
		java.sql.Date result;

		try {
			result = new java.sql.Date(stringToDate(strDate, format).getTime());
		} catch (Exception e) {
			// nothing I can do about it
			result = null;
		}

		return result;
	}

	private static java.util.Date alternativeGetUtilDate(java.util.Date date, String format) {
		String strDate = dateToString(date, format);
		java.util.Date result;

		try {
			result = new java.util.Date(stringToDate(strDate, format).getTime());
		} catch (Exception e) {
			result = null;
		}

		return result;
	}

	private static java.sql.Time alternativeGetTime(java.util.Date date, String format) {
		String strDate = dateToString(date, format);
		java.sql.Time result;

		try {
			result = new java.sql.Time(stringToDate(strDate, format).getTime());
		} catch (Exception e) {
			// nothing I can do about it
			result = null;
		}

		return result;
	}

	/**
	 * @return The current date, with no time component.
	 **/
	public static java.sql.Date getCurrentDate() {
		synchronized (_calendar) {
			java.sql.Date result;
			java.util.Date date = new Date();

			try {
				_calendar.setTime(date);
				_calendar.set(Calendar.MILLISECOND, 0);
				_calendar.set(Calendar.SECOND, 0);
				_calendar.set(Calendar.MINUTE, 0);
				_calendar.set(Calendar.HOUR_OF_DAY, 0);

				result = new java.sql.Date(_calendar.getTime().getTime());
			}
			/*
			 * Well, for unknown reasons if set SOME time zones IN THE CLIENT
			 * (for example GMT+13:00 Nuku'alofa) _calendar.getTime() throwing
			 * IllegalArgumentException. So here we go:
			 */
			catch (Exception ex) {
				result = alternativeGetDate(date, SLASHED_DATE);
			}
			return result;
		}
	}

	/**
	 * @return The time component of a date.
	 **/
	public static java.sql.Time getTime(Date date) {
		synchronized (_calendar) {
			java.sql.Time result = null;

			try {
				_calendar.setTime(date);
				_calendar.set(Calendar.DAY_OF_YEAR, 1);
				_calendar.set(Calendar.YEAR, 1970);
				result = new java.sql.Time(_calendar.getTime().getTime());
			} catch (Exception ex) {
				result = alternativeGetTime(date, SLASHED_DATE);
			}
			return result;

		}
	}

	/**
	 * Return the maximum value that this field could have, given the current
	 * date.
	 *
	 * @param date
	 *            - reference to date
	 * @param field
	 *            - the field to determine the maximum of
	 *
	 * @return - the maximum of the given field for the passed in date. -1 if
	 *         there are any errors
	 **/
	public static int getActualMaximum(Date date, int field) {
		synchronized (_calendar) {
			int result = -1;

			try {
				_calendar.setTime(date);
				result = _calendar.getActualMaximum(field);
			} catch (Exception ex) {
				result = -1;
			}
			return result;

		}
	}

	/**
	 * @return The date, with no time component.
	 **/
	public static java.sql.Date getDate(Date date) {
		synchronized (_calendar) {
			java.sql.Date result;

			try {
				_calendar.setTime(date);
				_calendar.set(Calendar.MILLISECOND, 0);
				_calendar.set(Calendar.SECOND, 0);
				_calendar.set(Calendar.MINUTE, 0);
				_calendar.set(Calendar.HOUR_OF_DAY, 0);

				result = new java.sql.Date(_calendar.getTime().getTime());

				/*
				 * Well, for unknown reasons if set SOME time zones IN THE
				 * CLIENT (for example GMT+13:00 Nuku'alofa) _calendar.getTime()
				 * throwing IllegalArgumentException. So here we go:
				 */
			} catch (Exception ex) {
				result = alternativeGetDate(date, SLASHED_DATE);
			}
			return result;

		}
	}

	/**
	 * @return The current time, with 'no' date component (set to January 1,
	 *         1970).
	 **/
	public static java.sql.Time getCurrentTime() {
		synchronized (_calendar) {
			java.sql.Time result = null;
			java.util.Date date = new java.util.Date();

			try {
				_calendar.setTime(date);
				_calendar.set(Calendar.DAY_OF_YEAR, 1);
				_calendar.set(Calendar.YEAR, 1970);
				result = new java.sql.Time(_calendar.getTime().getTime());

				/*
				 * Well, for unknown reasons if set SOME time zones IN THE
				 * CLIENT (for example GMT+13:00 Nuku'alofa) _calendar.getTime()
				 * throwing IllegalArgumentException. So here we go:
				 */
			} catch (Exception ex) {
				result = new java.sql.Time(date.getTime() - getCurrentDate().getTime());
			}

			return result;
		}
	}

	/**
	 * <p>
	 * Note that since a Timestamp inherits from java.util.Date, this method
	 * will also work on Timestamps.
	 * </p>
	 *
	 * <p>
	 * Known limitation: milliseconds will print as 0 if date is a
	 * <code>java.sql.Timestamp</code> object, because of inheritance weirdness
	 * between it and <code>java.util.Date</code>. This is a reasonably easy
	 * fix, not implemented because there's no need and it would marginally slow
	 * the code.
	 * </p>
	 *
	 * <p>
	 * For example, to display a date in the format "02/13/2001 12:45 PM", use
	 * <code>DateUtility.dateToString(myDate, DateUtility.SLASHED_DATE +
	 * " " + DateUtility.STANDARD_TIME);</code>
	 * </p>
	 *
	 * @see java.sql.Timestamp
	 * @param date
	 *            The date to be formatted.
	 * @param format
	 *            The format being applied to this date. Specifications can be
	 *            found in {@link java.util.SimpleDateFormat}. Alternately, you
	 *            can use one of the class variables.
	 * @return The date as a string, in the appropriate format.
	 **/
	public static String dateToString(Date date, String format) {
		synchronized (simpleDateFormat) {
			simpleDateFormat.applyPattern(format);
			return date != null ? simpleDateFormat.format(date) : "";
		}
	}

	/**
	 * <p>
	 * This method should <b>only</b> be used when your date may validly be
	 * null. Any mandatory date should use {@link #dateToString(Date, String)}.
	 * This is very important.
	 * </p>
	 *
	 * <p>
	 * Note that since a Timestamp inherits from java.util.Date, this method
	 * will also work on Timestamps.
	 * </p>
	 *
	 * <p>
	 * Known limitation: milliseconds will print as 0 if date is a
	 * <code>java.sql.Timestamp</code> object, because of inheritance weirdness
	 * between it and <code>java.util.Date</code>. This is a reasonably easy
	 * fix, not implemented because there's no need and it would marginally slow
	 * the code.
	 * </p>
	 *
	 * <p>
	 * For example, to display a date in the format "02/13/2001 12:45 PM", use
	 * <code>DateUtility.dateToString(myDate, DateUtility.SLASHED_DATE +
	 * " " + DateUtility.STANDARD_TIME);</code>
	 * </p>
	 *
	 * @see java.sql.Timestamp
	 * @param date
	 *            The date to be formatted.
	 * @param format
	 *            The format being applied to this date. Specifications can be
	 *            found in {@link java.util.SimpleDateFormat}. Preferably, you
	 *            can use one of the class variables.
	 * @param defaultIfNull
	 *            This string will be returned if Date is null.
	 * @return The date as a string, in the appropriate format.
	 **/
	public static String dateToString(Date date, String format, String defaultIfNull) {
		if (date == null) {
			return defaultIfNull;
		}
		synchronized (simpleDateFormat) {
			simpleDateFormat.applyPattern(format);
			return simpleDateFormat.format(date);
		}
	}

	/**
	 * @param date
	 *            A string representing a date.
	 * @param format
	 *            A string containing a simple date format, as defined in
	 *            {@link java.text.SimpleDateFormat}.
	 * @return A Date object corresponding to the date argument.
	 * @exception java.text.X12ParseException
	 *                when a Date can't be extracted from the date parameter
	 *                using the given format.
	 **/
	public static Date stringToDate(String date, String format) throws ParseException {
		synchronized (simpleDateFormat) {
			simpleDateFormat.applyPattern(format);
			return simpleDateFormat.parse(date);
		}
	}

	/**
	 * @param date
	 *            A string representing a date.
	 * @param format
	 *            A string containing a simple date format, as defined in
	 *            {@link java.text.SimpleDateFormat}.
	 * @return A Timestamp object corresponding to the date argument.
	 * @exception java.text.X12ParseException
	 *                when a Timestamp can't be extracted from the date
	 *                parameter using the given format.
	 **/
	public static Timestamp stringToTimestamp(String date, String format) throws ParseException {
		final Date d;
		synchronized (simpleDateFormat) {
			simpleDateFormat.applyPattern(format);
			d = simpleDateFormat.parse(date);
		}
		return new Timestamp(d.getTime());
	}

	/**
	 * @param date
	 *            A string representing a date.
	 * @param dateFormat
	 *            A string containing a simple date format, as defined in
	 *            {@link java.text.SimpleDateFormat}.
	 * @param desiredValue
	 *            Any one of the following variables from
	 *            {@link java.util.Calendar}: AM_PM, DATE, DAY_OF_MONTH,
	 *            DAY_OF_WEEK, DAY_OF_WEEK_IN_MONTH, DAY_OF_YEAR, DST_OFFSET,
	 *            ERA, HOUR, HOUR_OF_DAY, MILLISECOND, MINUTE, SECOND,
	 *            WEEK_OF_MONTH, WEEK_OF_YEAR, YEAR, ZONE_OFFSET. Note that
	 *            DST_OFFSET and ZONE_OFFSET are for the location of Virtual
	 *            Machine invoking this method.
	 * @return an int holding the specified value. If you're getting a month, be
	 *         aware that Jan=1, Feb=2, etc.
	 * @exception java.text.X12ParseException
	 *                when a Date can't be extracted from the date parameter
	 *                using the given format.
	 **/
	public static int getCalendarItem(String date, String dateFormat, int desiredValue) throws ParseException {
		final int value;
		synchronized (_calendar) {
			_calendar.setTime(stringToDate(date, dateFormat));
			value = _calendar.get(desiredValue);
		}

		// hack to fix stupid calendar, which counts everything from 1
		// except months, where January==0, February==1, .. December==11
		if (desiredValue == Calendar.MONTH) {
			return value + 1;
		} else {
			return value;
		}
	}

	/**
	 * <p>
	 * Note that since a Timestamp inherits from java.util.Date, this method
	 * will also work on Timestamps.
	 * </p>
	 *
	 * <p>
	 * Known limitation: milliseconds will print as 0 if date is a
	 * <code>java.sql.Timestamp</code> object, because of inheritance weirdness
	 * between it and <code>java.util.Date</code>. This is a reasonably easy
	 * fix, not implemented because there's no need and it would marginally slow
	 * the code.
	 * </p>
	 *
	 * @param date
	 *            The java.util.Date object from which we desire a value.
	 * @param desiredValue
	 *            Any one of the following variables from
	 *            {@link java.util.Calendar}: AM_PM, DATE, DAY_OF_MONTH,
	 *            DAY_OF_WEEK, DAY_OF_WEEK_IN_MONTH, DAY_OF_YEAR, DST_OFFSET,
	 *            ERA, HOUR, HOUR_OF_DAY, MILLISECOND, MINUTE, SECOND,
	 *            WEEK_OF_MONTH, WEEK_OF_YEAR, YEAR, ZONE_OFFSET. Note that
	 *            DST_OFFSET and ZONE_OFFSET are for the location of Virtual
	 *            Machine invoking this method.
	 * @return an int holding the specified value. If you're getting a month, be
	 *         aware that Jan=1, Feb=2, etc.
	 **/
	public static int getCalendarItem(Date date, int desiredValue) {
		final int value;
		synchronized (_calendar) {
			_calendar.setTime(date);
			value = _calendar.get(desiredValue);
		}

		// hack to fix stupid calendar, which counts everything from 1
		// except months, where January==0, February==1, .. December==11
		if (desiredValue == Calendar.MONTH) {
			return value + 1;
		} else {
			return value;
		}
	}

	/**
	 * @param date
	 *            the Date whose value we wish to modify. It will <b>not<b> be
	 *            changed as a side effect of this method.
	 * @param field
	 *            Any one of the following variables from
	 *            {@link java.util.Calendar}: AM_PM, DATE, DAY_OF_MONTH,
	 *            DAY_OF_WEEK, DAY_OF_WEEK_IN_MONTH, DAY_OF_YEAR, DST_OFFSET,
	 *            ERA, HOUR, HOUR_OF_DAY, MILLISECOND, MINUTE, SECOND,
	 *            WEEK_OF_MONTH, WEEK_OF_YEAR, YEAR, ZONE_OFFSET. Note that
	 *            DST_OFFSET and ZONE_OFFSET are for the location of Virtual
	 *            Machine invoking this method.
	 * @param amount
	 *            the size of the change to be made to this field, either
	 *            positive or negative. Note that if you push a field past valid
	 *            values, the next highest value will also increment. So, for
	 *            example, if the month of your Timestamp is December, and you
	 *            increment Calendar.MONTH by one, your Timestamp will now be
	 *            for January of the next year.
	 **/
	public static Date modifyDateValue(Date date, int field, int amount) {
		synchronized (_calendar) {
			_calendar.setTime(date);
			_calendar.add(field, amount);
			return _calendar.getTime();
		}
	}

	/**
	 * @param date
	 *            the Timestamp whose value we wish to modify. It will <b>not
	 *            <b> be changed as a side effect of this method.
	 * @param field
	 *            Any one of the following variables from
	 *            {@link java.util.Calendar}: AM_PM, DATE, DAY_OF_MONTH,
	 *            DAY_OF_WEEK, DAY_OF_WEEK_IN_MONTH, DAY_OF_YEAR, DST_OFFSET,
	 *            ERA, HOUR, HOUR_OF_DAY, MILLISECOND, MINUTE, SECOND,
	 *            WEEK_OF_MONTH, WEEK_OF_YEAR, YEAR, ZONE_OFFSET. Note that
	 *            DST_OFFSET and ZONE_OFFSET are for the location of Virtual
	 *            Machine invoking this method.
	 * @param amount
	 *            the size of the change to be made to this field, either
	 *            positive or negative. Note that if you push a field past valid
	 *            values, the next highest value will also increment. So, for
	 *            example, if the month of your Timestamp is December, and you
	 *            increment Calendar.MONTH by one, your Timestamp will now be
	 *            for January of the next year.
	 **/
	public static Timestamp modifyTimestampValue(Timestamp date, int field, int amount) {
		final Date d;
		synchronized (_calendar) {
			_calendar.setTime(date);
			_calendar.add(field, amount);
			d = _calendar.getTime();
		}
		return new Timestamp(d.getTime());
	}

	/**
	 * This method generates the offset between the current time zone and a
	 * given time zone on the specific date. The date is important because some
	 * time zones will apply Daylight Savings Time, while others will not.
	 *
	 * @param timeZone
	 *            the time zone whose offset from ours we want
	 * @param date
	 *            the date at which to determine the offset
	 * @return the number of hours to add to my local time zone to get the time
	 *         at the given time zone on the given date
	 **/
	public static int getTimeZoneOffset(TimeZone timeZone, Date date) {
		TimeZone localTimeZone = _calendar.getTimeZone();
		int diff = timeZone.getRawOffset() - localTimeZone.getRawOffset();
		if (timeZone.inDaylightTime(date)) {
			diff += DateUtility.MILLIS_PER_HOUR;
		}
		if (localTimeZone.inDaylightTime(date)) {
			diff -= DateUtility.MILLIS_PER_HOUR;
		}
		return diff / DateUtility.MILLIS_PER_HOUR;
	}

	/**
	 * @param date
	 *            the date whose age you need.
	 * @return how many years have passed since the given day.
	 * @throws NullPointerException
	 *             Thrown if date is null.
	 **/
	public static int getAge(Date date) throws NullPointerException {
		return DateUtility.getAge(Calendar.YEAR, date);
	}

	/**
	 * @param measureBy
	 *            How should I measure age? Accepts either Calendar.DATE or
	 *            Calendar.YEAR. Any other value causes an
	 *            IllegalArgumentException to be thrown.
	 * @param date
	 *            the date whose age you need. Note that since Timestamp is a
	 *            subclass of Date, this parameter may be a Timestamp object.
	 * @return how much time has passed since the given date.
	 * @throws NullPointerException
	 *             if date is null.
	 * @throws IllegalArgumentException
	 *             if measureBy is not Calendar.DATE or Calendar.YEAR
	 **/
	public static int getAge(int measureBy, Date date) throws NullPointerException, IllegalArgumentException {

		/* This method is pretty ugly and could use fixing up. */
		if (date == null) {
			throw new NullPointerException("The date may not be null.");
		}

		switch (measureBy) {
		case Calendar.DATE:
			/*
			 * THIS IS UGLY -- FIX
			 */
			return (int) (((((System.currentTimeMillis() - date.getTime()) / 1000) / 60) / 60) / 24);
		case Calendar.YEAR:
			final int dateYear;
			final int dateMonth;
			final int dateDay;
			int age;
			final int nowMonth;
			final int nowDay;

			synchronized (_calendar) {
				_calendar.setTime(date);
				dateYear = _calendar.get(Calendar.YEAR);
				dateMonth = _calendar.get(Calendar.MONTH);
				dateDay = _calendar.get(Calendar.DAY_OF_MONTH);

				_calendar.setTime(new Date());
				age = _calendar.get(Calendar.YEAR) - dateYear;
				nowMonth = _calendar.get(Calendar.MONTH);
				nowDay = _calendar.get(Calendar.DAY_OF_MONTH);
			}

			if (nowMonth < dateMonth || (nowMonth == dateMonth && nowDay < dateDay))
				age--;
			return age;
		default:
			throw new IllegalArgumentException(measureBy + "is an invalid " + "'measureBy' parameter.");
		}
	}

	/**
	 * @param date
	 *            the date whose age you need.
	 * @return how many years have passed since the given day. If the number of
	 *         years is less than two, return instead the number of months since
	 *         the given day. If the number of months is less than two, return
	 *         instead of the number of days. The format is either "## Yrs",
	 *         "## Mos", "## Days". If the date is null, return zero length
	 *         string.
	 *
	 *         Note, the child is 1 day old on the day he/she is born.
	 *
	 **/
	public static String getAgeString(Date date) {
		if (date == null) {
			return "";
		}
		final int dateYear;
		final int dateMonth;
		final int dateDayOfMonth;
		final int dateDayOfYear;
		final int nowYear;
		final int nowMonth;
		final int nowDayOfMonth;
		final int nowDayOfYear;
		final int maxDayInYear;

		synchronized (_calendar) {
			_calendar.setTime(new Date());
			nowYear = _calendar.get(Calendar.YEAR);
			nowMonth = _calendar.get(Calendar.MONTH);
			nowDayOfMonth = _calendar.get(Calendar.DAY_OF_MONTH);
			nowDayOfYear = _calendar.get(Calendar.DAY_OF_YEAR);

			_calendar.setTime(date);
			dateYear = _calendar.get(Calendar.YEAR);
			dateMonth = _calendar.get(Calendar.MONTH);
			dateDayOfMonth = _calendar.get(Calendar.DAY_OF_MONTH);
			dateDayOfYear = _calendar.get(Calendar.DAY_OF_YEAR);
			maxDayInYear = _calendar.getActualMaximum(Calendar.DAY_OF_YEAR);
		}
		int age = nowYear - dateYear;
		if (nowMonth < dateMonth || (nowMonth == dateMonth && nowDayOfMonth < dateDayOfMonth))
			age--;
		if (age < 0)
			return "";
		else if (age >= 2)
			return age + " Yrs";
		else {
			age = (nowYear - dateYear) * 12 + nowMonth - dateMonth;
			if (nowDayOfMonth < dateDayOfMonth)
				age--;
			if (age >= 1)
				return age + " Mos";
			else {
				age = nowDayOfYear - dateDayOfYear + 1; // The baby is 1 day old
														// on the day of birth
				if (dateYear < nowYear)
					age += maxDayInYear;
				if (age > 1)
					return age + " Days";
				else if (age == 1)
					return "1 Day";
				else
					return "";
			}
		}
	}

	/**
	 * Gets the default <code>TimeZone</code> for this host. The source of the
	 * default <code>TimeZone</code> may vary with implementation.
	 * 
	 * @return a default <code>TimeZone</code>.
	 * @see #setDefaultTimeZone
	 */
	public static TimeZone getDefaultTimeZone() {
		return TimeZone.getDefault();
	}

	/**
	 * Sets the <code>TimeZone</code> that is returned by the
	 * <code>getDefaultTimeZone</code> method. The specified
	 * <code>TimeZone</code> must be one of the supported <code>TimeZone</code>
	 * s.
	 * 
	 * @param zone
	 *            the new default time zone
	 * @see #getDefaultTimeZone
	 */
	public static void setDefaultTimeZone(TimeZone timeZone) {
		TimeZone.setDefault(timeZone);
		synchronized (simpleDateFormat) {
			simpleDateFormat.setTimeZone(timeZone);
		}
		synchronized (_calendar) {
			_calendar.setTimeZone(timeZone);
		}
	}

	/**
	 * Is the date1 the same as the date2 same. Ingore the time part.
	 * 
	 * @param date1
	 *            the given date 1 to be checked.
	 * @param date2
	 *            the give data 2 to be checked.
	 * @return true if two dates are the same or both dates are null.
	 */
	public static boolean isSameDate(Date date1, Date date2) {
		boolean flag = false;
		if (date1 == null) {
			if (date2 == null) {
				flag = true;
			}
		} else if (date2 != null) {
			String str1 = dateToString(date1, SLASHED_DATE);
			String str2 = dateToString(date2, SLASHED_DATE);
			if (str1.equals(str2)) {
				flag = true;
			}
		}

		return flag;
	}

	/**
	 * Describe <code>isSameDate</code> method here.
	 *
	 * @param date1
	 *            a <code>DateWrapper</code> value
	 * @param date2
	 *            a <code>DateWrapper</code> value
	 * @return a <code>boolean</code> value
	 * @deprectaed use the equals method on <code>DateWrapper</code>
	 */
	public static boolean isSameDate(DateWrapper date1, DateWrapper date2) {
		return isSameDate(((date1 != null) ? date1.asDate() : null), ((date2 != null) ? date2.asDate() : null));
	}

	public static Timestamp getOriginalDate(Timestamp input, TimeZone serverZone) {
		return (Timestamp) getOriginalDate((Date) input, serverZone);
	}

	/**
	 * Determines if the <code>date</code> occurred before the fixed point in
	 * time that is represented by the <code>DateWrapper</code>. The answer to
	 * this question may be different depending upon the timezone of the
	 * requestor.
	 *
	 * @param when
	 *            a <code>DateWrapper</code> value
	 * @param date
	 *            a <code>Date</code> value
	 * @param timeZone
	 *            a <code>TimeZone</code> value
	 * @return a <code>boolean</code> value
	 */
	public static boolean isBefore(final DateWrapper when, final Date date, final TimeZone timeZone) {
		if (when == null)
			throw new IllegalArgumentException("Date wrapper can not be null");

		if (date == null)
			throw new IllegalArgumentException("Date can not be null");

		return getOriginalDate(date, timeZone).getTime() < when.asDate().getTime();
	}

	/**
	 * Determines if the <code>date</code> occurred after the fixed point in
	 * time that is represented by the <code>DateWrapper</code>. The answer to
	 * this question may be different depending upon the timezone of the
	 * requestor.
	 *
	 * @param when
	 *            a <code>DateWrapper</code> value
	 * @param date
	 *            a <code>Date</code> value
	 * @param timeZone
	 *            a <code>TimeZone</code> value
	 * @return a <code>boolean</code> value
	 */
	public static boolean isAfter(final DateWrapper when, final Date date, final TimeZone timeZone) {
		if (when == null)
			throw new IllegalArgumentException("Date wrapper can not be null");

		if (date == null)
			throw new IllegalArgumentException("Date can not be null");

		return getOriginalDate(date, timeZone).getTime() > when.asDate().getTime();
	}

	/**
	 * Factory method to return a <code>DateWrapper</code>.
	 *
	 * @param in
	 *            a <code>String</code> value
	 * @return a <code>DateWrapper</code> value
	 */
	public static DateWrapper DateWrapperInstance(String in) {
		return in == null ? null : new DateWrapper(in);
	}

	/**
	 * Factory method to return a <code>DateWrapper</code>.
	 *
	 * @param in
	 *            a <code>Date</code> value
	 * @return a <code>DateWrapper</code> value
	 */
	public static DateWrapper DateWrapperInstance(Date in) {
		return in == null ? null : new DateWrapper(in);
	}

	/**
	 * Factory method to return a <code>DateWrapper</code>.
	 *
	 * @return a <code>DateWrapper</code> value
	 */
	public static DateWrapper DateWrapperInstance() {
		return new DateWrapper();
	}

	/**
	 *
	 * Get Date as it would be in the original time zone, e.g. without
	 * converting to local time zone. This is a work around to avoid rolling a
	 * date to the previous day when the time is 12:00 am and the time zone
	 * adjustment is negative
	 *
	 */
	public static Date getOriginalDate(Date input, TimeZone serverZone) {

		if (input == null || serverZone == null)
			return input;

		int serverOffset = serverZone.getOffset(input.getTime());
		int clientOffset = TimeZone.getDefault().getOffset(input.getTime());

		if (clientOffset == serverOffset)
			return input;

		Date oriDate = input;
		if (input instanceof Date)
			oriDate = new Date(input.getTime() + serverOffset - clientOffset);
		if (input instanceof Timestamp)
			oriDate = new Timestamp(input.getTime() + serverOffset - clientOffset);
		return oriDate;
	}

	/**
	 * Convert date of birth Date Object from orgTimeZone to the given
	 * toTimeZone. The date string for the return Date Object in the toTimeZone
	 * is the same as the date string for the orignal date of birth in the
	 * orginal time zone.
	 *
	 * @param dob
	 *            the original date of birth in Date
	 * @param orgTimeZone
	 *            the orginal time zone
	 * @param toTimeZone
	 *            the to time zone.
	 * @return the date of birth Date value in the toTimeZone.
	 */
	public static Date convertDOB(Date dob, TimeZone orgTimeZone, TimeZone toTimeZone) {
		if (orgTimeZone == null || dob == null) {
			return dob;
		}
		if (toTimeZone == null) {
			toTimeZone = TimeZone.getDefault();
		}

		if (!(toTimeZone.getRawOffset() == orgTimeZone.getRawOffset())) {
			TimeZone oldTimeZone = simpleDateFormat.getTimeZone();
			synchronized (simpleDateFormat) {
				try {
					// Try to keep the oldTimeZone and set it back after done.
					oldTimeZone = simpleDateFormat.getTimeZone();

					simpleDateFormat.setTimeZone(orgTimeZone);
					simpleDateFormat.applyPattern(SLASHED_DATE);
					String dateStr = simpleDateFormat.format(dob);
					simpleDateFormat.setTimeZone(toTimeZone);
					return simpleDateFormat.parse(dateStr);
				} catch (ParseException pe) {
					// Should not be here. If here just return original value
					return dob;
				} finally {
					simpleDateFormat.setTimeZone(oldTimeZone);
				}
			}
		}
		return dob;
	}

	/**
	 * Useful if you're comparing against a date/time obtained via a UI widget
	 * that only has minute precision.
	 * 
	 * @return a timestamp, with seconds / milliseconds set to zero.
	 **/
	public static Timestamp truncateSeconds(Timestamp date) {
		synchronized (_calendar) {
			_calendar.setTime(date);
			_calendar.set(Calendar.MILLISECOND, 0);
			_calendar.set(Calendar.SECOND, 0);

			return new Timestamp(_calendar.getTime().getTime());
		}
	}

	/**
	 * This method calculates the date difference in terms of argument:
	 * 'calculateBy'.
	 *
	 * @param biggerDate
	 *            - date later in the calendar.
	 * @param smallerDate
	 *            - date earlier in the calendar.
	 * @param calculateBy
	 *            - Valid values are Calendar.MILLISECONDS, Calendar.SECONDS,
	 *            Calendar.MINUTE, Calendar.HOUR, Calendar.DATE, Calendar.MONTH
	 *            and Calendar.YEAR.
	 *            <P>
	 *            For Calendar.DATE, Calendar.MONTH and CALENDAR.YEAR, the time
	 *            components of the dates are ignored.
	 *            </P>
	 *            <P>
	 *            A 30 day month and a 365 day year is assumed for calculated
	 *            date difference by Calendar.MONTH and Calendar.YEAR
	 *            respectively.
	 *            </P>
	 *
	 *
	 * @return - difference in date as per the calculateBy argument. If
	 *         biggerDate is smaller than smallerDate, difference is returned in
	 *         negative.
	 */
	public static long getDateDiff(Date biggerDate, Date smallerDate, int calculateBy) throws Exception {

		// argument check
		if (biggerDate == null || smallerDate == null) {
			throw new IllegalArgumentException("Difference cannot be calculated because date was passed as NULL");
		}

		// initialize
		long biggerTime = 0, smallerTime = 0, diff = 0;

		// proceed with calculation
		switch (calculateBy) {
		case Calendar.MILLISECOND:
			biggerTime = biggerDate.getTime();
			smallerTime = smallerDate.getTime();
			diff = biggerTime - smallerTime;
			break;
		case Calendar.MINUTE:
			biggerTime = biggerDate.getTime();
			smallerTime = smallerDate.getTime();
			diff = (biggerTime - smallerTime) / DateUtility.MILLIS_PER_MINUTE;
			break;
		case Calendar.HOUR:
			biggerTime = biggerDate.getTime();
			smallerTime = smallerDate.getTime();
			diff = (biggerTime - smallerTime) / DateUtility.MILLIS_PER_HOUR;
			break;
		case Calendar.DATE:
			// Ignore time part
			biggerDate = DateUtility.getDate(biggerDate);
			smallerDate = DateUtility.getDate(smallerDate);
			// Do calculation
			biggerTime = biggerDate.getTime();
			smallerTime = smallerDate.getTime();
			// Round this result due to the two days of daylight savings time,
			// specifically,
			// this really addresses the spring DST day that has 23 hours.
			diff = Math.round((biggerTime - smallerTime) / (double) DateUtility.MILLIS_PER_DAY);
			break;
		case Calendar.MONTH:
			// Ignore time part
			biggerDate = DateUtility.getDate(biggerDate);
			smallerDate = DateUtility.getDate(smallerDate);
			// Do calculation
			biggerTime = biggerDate.getTime();
			smallerTime = smallerDate.getTime();
			diff = (biggerTime - smallerTime) / DateUtility.MILLIS_PER_DAY;
			diff = diff / 30;
			break;
		case Calendar.YEAR:
			// Ignore time part
			biggerDate = DateUtility.getDate(biggerDate);
			smallerDate = DateUtility.getDate(smallerDate);
			// Do calculation
			biggerTime = biggerDate.getTime();
			smallerTime = smallerDate.getTime();
			diff = (biggerTime - smallerTime) / DateUtility.MILLIS_PER_DAY;
			diff = diff / 365;
			break;
		default:
			throw new IllegalArgumentException(
					"Date differences can only be calculated by MILLISECONDS, SECONDS, MINUTE, HOUR, DATE, MONTH and YEAR");
		}

		return diff;
	}

	/**
	 * Convert a Date object that we do not want time component information for
	 * into a string in the form of 'ddMMyyyy' for transport to other
	 * application tiers.
	 *
	 * @param date
	 *            Date
	 * @return String
	 */
	public static String convertDateWithoutTimeForTransport(Date dateObj) {
		if (dateObj == null)
			return null;

		return dateToString(dateObj, POSITIONAL_DATE);
	}

	public static Timestamp asTimestamp(DateWrapper date1) {
		return ((date1 != null) ? new Timestamp(date1.asDate().getTime()) : null);
	}

	/**
	 * Create a Date object that we do not want time component information for
	 * from a string in the form of 'ddMMyyyy' for display on the client tier/
	 *
	 * @param date
	 *            String
	 * @return Date
	 */
	public static Date convertDateWithoutTimeForDisplay(String dateObj) {
		if (dateObj == null)
			return null;

		if (dateObj.length() != 8) {
			throw new IllegalStateException(
					"Date string [" + dateObj + "] is " + ((dateObj == null) ? "null" : "of incorrect length"));
		}

		try {
			Long.parseLong(dateObj);
		} catch (Exception e) {
			throw new IllegalStateException("Date string is not completely numeric");
		}

		String daysStr = dateObj.substring(0, 2);
		int days = Integer.parseInt(daysStr);
		String monthStr = dateObj.substring(2, 4);
		int month = Integer.parseInt(monthStr);
		String yearStr = dateObj.substring(4);
		int year = Integer.parseInt(yearStr);

		Calendar rtnCal = Calendar.getInstance();
		rtnCal.clear();

		rtnCal.set(year, month - 1, days, 0, 0, 0);

		return rtnCal.getTime();
	}

	public static class DateWrapper implements java.io.Serializable {

		private static final long serialVersionUID = -1;
		//
		// Instance Data
		//

		private final String dateData;

		//
		// Constructors
		//

		/**
		 * Creates a new <code>DateWrapper</code> instance.
		 *
		 * @param data
		 *            a <code>Date</code> value
		 * @deprecated This will soon be made <code>protected</code>. Use
		 *             DateUtility.DateWrapperInstance(Date)
		 */
		@Deprecated
		public DateWrapper(Date data) {
			if (data == null)
				throw new IllegalStateException("Date can not be null.");
			dateData = convertDateWithoutTimeForTransport(data);
		}

		/**
		 * Creates a new <code>DateWrapper</code> instance.
		 *
		 * @param data
		 *            a <code>String</code> value
		 * @deprecated This will soon be made <code>protected</code>. Use
		 *             DateUtility.DateWrapperInstance(String)
		 */
		@Deprecated
		public DateWrapper(String data) {
			validate(data);
			dateData = data;
		}

		/**
		 * Creates a new <code>DateWrapper</code> instance set to the current
		 * date.
		 * 
		 * @deprecated This will soon be made <code>protected</code>. Use
		 *             DateUtility.DateWrapperInstance()
		 */
		@Deprecated
		public DateWrapper() {
			dateData = DateUtility.convertDateWithoutTimeForTransport(new Date());
		}

		//
		// Data Access
		//

		public Date asDate() {
			return convertDateWithoutTimeForDisplay(dateData);
		}

		public String asString() {
			return dateData;
		}

		@Override
		public String toString() {
			return asString();
		}

		//
		// Object Overrides
		//

		@Override
		public int hashCode() {
			if (dateData == null)
				return 0;
			return dateData.hashCode();
		}

		@Override
		public boolean equals(Object o) {
			if (this == o)
				return true;

			if (!(o instanceof DateWrapper))
				return false;

			DateWrapper dw = (DateWrapper) o;

			return dateData.equals(dw.asString());
		}

		//
		// Helpers
		//

		/**
		 * Return a DateWrapper with a value of this + days.
		 *
		 * @param days
		 *            an <code>int</code> value
		 * @return a <code>DateString</code> value
		 */
		public DateWrapper addDays(int days) {
			return new DateWrapper(DateUtility.modifyDateValue(asDate(), Calendar.DATE, days));
		}

		/**
		 * Implementation of java.lang.Comparable
		 *
		 * @param object
		 *            an <code>Object</code> value
		 * @return an <code>int</code> value
		 */
		public final int compareTo(final Object o) {
			DateWrapper dw = (DateWrapper) o;

			return asDate().compareTo(dw.asDate());
		}

		private void validate(String data) {
			if (data == null)
				throw new IllegalStateException("Date String can not be null");
			try {
				stringToDate(data, POSITIONAL_DATE);
			} catch (ParseException pe) {
				throw new IllegalStateException(data + " is invalid");
			}
		}
	}

	public static String dateToString(DateUtility.DateWrapper date, String format) {
		synchronized (simpleDateFormat) {
			simpleDateFormat.applyPattern(format);
			return date != null ? simpleDateFormat.format(date.asDate()) : "";
		}
	}

	public static String createUsageDaysString(Date lastFillDate, Number daysSupply, String prefix, String separator) {
		if (lastFillDate == null || daysSupply == null)
			return "";
		try {
			long daysSinceLastFill = getDateDiff(new Date(), lastFillDate, Calendar.DATE);
			return prefix + String.valueOf(daysSinceLastFill) + separator + daysSupply + " Days";
		} catch (Exception e) {
			return "";
		}

	}

	/**
	 * Returns yesterday's date
	 * 
	 * @return
	 */
	public static java.util.Date getPreviousDate() {
		synchronized (_calendar) {
			java.util.Date result;
			java.util.Date date = new Date();

			try {
				_calendar.add(Calendar.DATE, -1);
				_calendar.set(Calendar.MILLISECOND, 0);
				_calendar.set(Calendar.SECOND, 0);
				_calendar.set(Calendar.MINUTE, 0);
				_calendar.set(Calendar.HOUR_OF_DAY, 0);

				result = new java.util.Date(_calendar.getTime().getTime());
			}
			/*
			 * Well, for unknown reasons if set SOME time zones IN THE CLIENT
			 * (for example GMT+13:00 Nuku'alofa) _calendar.getTime() throwing
			 * IllegalArgumentException. So here we go:
			 */
			catch (Exception ex) {
				result = alternativeGetUtilDate(date, SLASHED_DATE);
			}
			return result;
		}
	}

	/**
	 * Parses the date time string If for some reason the parse fails, it
	 * returns present date and time
	 * 
	 * @param time
	 * @return Calendar
	 */
	public static Calendar getDateTime(String time) {
		Calendar calendar = null;
		if (time != null && time.length() > 0) {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.S'Z'");
			Date date;
			try {
				date = dateFormat.parse(time);
			} catch (ParseException e) {
				e.printStackTrace();
				date = new Date();
			}
			calendar = Calendar.getInstance();
			calendar.setTime(date);
		}
		return calendar;
	}

	/**
	 * Gets current date time in UTC format
	 * 
	 * @return Calendar
	 */
	public static Calendar getCurrentDateTimeInUTC() {
		return Calendar.getInstance(TimeZone.getTimeZone("UTC"));
	}
}
