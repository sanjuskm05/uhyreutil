package com.uhyre.common.util;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.uhyre.common.dto.GeneralResponseDto;

public class ResponseUtil {

	public static final String FAILURE_STATUS = "Failure";
	public static final String BAD_REQUEST = "BadRequest";
	public static final String INTERNAL_SERVER_ERROR = "InternalServerError";
	public static final String NOT_FOUND = "NotFound";
	public static final String SUCCESS = "Success";
	public static final String PARAM_MISSING = "paramMissing";
	
	public static ResponseEntity<GeneralResponseDto> createNotFoundError(String message) {
		GeneralResponseDto error = new GeneralResponseDto();
        error.setStatus(NOT_FOUND);
        if(message != null && !message.isEmpty()){
        	error.setMessage(message);
        }else{
        	error.setMessage("Record not found");
        }
        return new ResponseEntity<GeneralResponseDto>(error, HttpStatus.NOT_FOUND);
    }

	public static ResponseEntity<GeneralResponseDto> createBadRequestError(String message) {
		GeneralResponseDto error = new GeneralResponseDto();
        error.setStatus(BAD_REQUEST);
        if(message != null && !message.isEmpty()){
        	error.setMessage(message);
        }else{
        	error.setMessage(BAD_REQUEST);
        }
        return new ResponseEntity<GeneralResponseDto>(error,HttpStatus.BAD_REQUEST);
	}
	
	public static ResponseEntity<GeneralResponseDto> createInternalServerError(Throwable t) {
        GeneralResponseDto error = new GeneralResponseDto();
        error.setStatus(INTERNAL_SERVER_ERROR);
       	error.setMessage(t.getMessage());
        return new ResponseEntity<GeneralResponseDto>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}


	public static ResponseEntity<GeneralResponseDto> createFailure(String message) {
		GeneralResponseDto error = new GeneralResponseDto();
        error.setStatus(FAILURE_STATUS);
        if(message != null && !message.isEmpty()){
        	error.setMessage(message);
        }else{
        	error.setMessage("Failure");
        }
        return new ResponseEntity<GeneralResponseDto>(error, HttpStatus.OK);
    }


	
	public static ResponseEntity<GeneralResponseDto> paramMissing(String message) {
		GeneralResponseDto error = new GeneralResponseDto();
        error.setStatus(PARAM_MISSING);
        if(message != null && !message.isEmpty()){
        	error.setMessage(message);
        }else{
        	error.setMessage("Failure");
        }
        return new ResponseEntity<GeneralResponseDto>(error, HttpStatus.OK);
    }
	
	public static ResponseEntity<GeneralResponseDto> createSuccess(String message) {
		GeneralResponseDto successDto = new GeneralResponseDto();
		successDto.setStatus(SUCCESS);
        if(message != null && !message.isEmpty()){
        	successDto.setMessage(message);
        }else{
        	successDto.setMessage("Success");
        }
        return new ResponseEntity<GeneralResponseDto>(successDto, HttpStatus.OK);
	}
	
	public static ResponseEntity<GeneralResponseDto> createSuccess(String message, String registrationDeviceToken) {
		GeneralResponseDto successDto = new GeneralResponseDto();
		successDto.setStatus(SUCCESS);
        if(message != null && !message.isEmpty()){
        	successDto.setMessage(message);
        }else{
        	successDto.setMessage("Success");
        }
        return new ResponseEntity<GeneralResponseDto>(successDto, HttpStatus.OK);
	}
}
