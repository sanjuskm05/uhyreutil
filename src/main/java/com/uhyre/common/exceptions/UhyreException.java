package com.uhyre.common.exceptions;

/**
 * 
 * @author Sanjay
 *
 */
@SuppressWarnings("serial")
public class UhyreException extends Exception{
	public UhyreException() {}
	
	public UhyreException(String message)
    {
       super(message);
    }
}
